package com.example.barut.petmatch;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.barut.petmatch.dataobject.Pet;
import com.example.barut.petmatch.request.FindMatchRequest;
import com.example.barut.petmatch.request.GetMyMatchesRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyMatches extends BaseFragmentActivity implements AsyncListener{
    public static ArrayList<Pet> petArrayList=new ArrayList<>();
    LinearLayout layoutic;
    int petid=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_matches);
        header_logo_layout.setVisibility(View.VISIBLE);
        headerMenu.setVisibility(View.VISIBLE);
        headerLayout.setClickable(true);
        setSlidingMenu();
        layoutic=(LinearLayout)findViewById(R.id.layout_ic_matches);
        layoutic.removeAllViews();
        petArrayList.removeAll(petArrayList);
        new GetMyMatchesRequest(MyMatches.this);

    }

    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue, String extraData) {
        JSONObject jsonObject=(JSONObject) obj;
        try {
            if (jsonObject.getString("response") == "1") {
                JSONArray arr=jsonObject.getJSONArray("list");
                for(int i=0;i<arr.length();i++) {
                    String name=arr.getJSONObject(i).getString("petName");
                    String photopath=arr.getJSONObject(i).getString("petProfilePhotoPath");
                    String id=arr.getJSONObject(i).getString("petID");
                    Pet pet=new Pet(name,photopath,id);
                    petArrayList.add(pet);
                }
                for(int i=0;i<petArrayList.size();i++) {
                    layoutic.addView(MatchListObject.getObject(MyMatches.this, petArrayList.get(i).petName,
                            petArrayList.get(i).petProfilePhotoPath,
                            petArrayList.get(i).id));
                }

            }
        } catch(JSONException j) {
            j.printStackTrace();
        }
    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {

    }
}
