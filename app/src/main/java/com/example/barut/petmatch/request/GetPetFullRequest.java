package com.example.barut.petmatch.request;

import android.os.AsyncTask;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lENOVO on 31.05.2017.
 */

public class GetPetFullRequest extends AsyncTask<Void, Void, String> {
    private String petID="";
    private String token="";
    AsyncListener asc;

    public GetPetFullRequest(String petID,AsyncListener asc) {
        this.petID = petID;
        this.asc=asc;
        execute();
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("petID",petID);
            jsonObject.put("token", Utils.token);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_PETS + "getPetInfo", jsonObject);

    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.GET_PET_FULL, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.GET_PET_FULL);
        }
        super.onPostExecute(result);
    }
}
