package com.example.barut.petmatch;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import loadimage.ImageLoader;

public class PhotoDetailActivity extends BaseFragmentActivity {
    ImageView newsImageView;
    TextView newsTextView;
    TextView newsTitleView;
    TextView newsDateView;
    private int newsID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        Intent intent = getIntent();
        //this.newsID=intent.getIntExtra("newsID",0);
        //String newsTitleNew= intent.getStringExtra("newsTitle");
        //String newsTextNew= intent.getStringExtra("newsText");
        //String newsDateNew= intent.getStringExtra("newsDate");
        String newsImage= intent.getStringExtra("path");
        newsImageView=(ImageView)findViewById(R.id.newsImage);
        newsTitleView=(TextView)findViewById(R.id.newsTitle);
        newsTextView=(TextView)findViewById(R.id.newsText);
        newsDateView=(TextView)findViewById((R.id.newsDate));
        // newsImageView.setImageResource(newsImageId);
        ImageLoader imgLoader = new ImageLoader(this);
        imgLoader.DisplayImage(newsImage,  newsImageView);
        newsDateView.setText("21.08.2008");
        newsTitleView.setText("Pamuk sahibini buldu!");
        newsTextView.setText("PetMatch sayesinde bir köpek daha sahibini buldu. Kendine yuva arayan barınak köpeği Pamuk yeni evinden memnun.");


    }
}
