package com.example.barut.petmatch.service;



import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServiceManager {

	
	public static String sendGET(String URLpath){


        HttpURLConnection urlConnection=null;
        StringBuilder builder=null;
        try {
            URL url=new URL(URLpath);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(false);
            BufferedInputStream in=new BufferedInputStream(urlConnection.getInputStream());
            builder = new StringBuilder();
            int b = 0;
            while((b=in.read())!=-1)
            {
                builder.append((char)b);
            }
        }
        catch(Exception e){

        }
        finally{
            urlConnection.disconnect();
            return builder.toString();
        }

    }
	

	
	public static String sendPOST(String URLpath,JSONObject obj) {

        HttpURLConnection urlConnection=null;
        StringBuilder builder=null;
        try {
            URL url=new URL(URLpath);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type","application/json");
            String params =obj.toString();

            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            out.write(params.getBytes());
            out.flush();

            out.close();
            BufferedInputStream in=new BufferedInputStream(urlConnection.getInputStream());
            builder = new StringBuilder();
            int b = 0;
            while((b=in.read())!=-1)
            {
                builder.append((char)b);
            }


        }
        catch(Exception e){

        }
        finally{
            urlConnection.disconnect();
            return builder.toString();
        }

    }
}
