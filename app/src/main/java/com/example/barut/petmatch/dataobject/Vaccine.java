package com.example.barut.petmatch.dataobject;

/**
 * Created by barut on 21.03.2017.
 */
public class Vaccine {
   String  _id;			// string
   String vaccineName;		// string
   String vaccineDate;

    public Vaccine() {}
    public Vaccine(String vaccineName, String vaccineDate) {
        this.vaccineName = vaccineName;
        this.vaccineDate = vaccineDate;
    }

    public Vaccine(String _id, String vaccineName, String vaccineDate) {
        this._id = _id;
        this.vaccineName = vaccineName;
        this.vaccineDate = vaccineDate;
    }
}
