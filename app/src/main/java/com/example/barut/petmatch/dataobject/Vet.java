package com.example.barut.petmatch.dataobject;

/**
 * Created by barut on 21.03.2017.
 */
public class Vet {

       String _id ;					// string
        String vetName;				// string
        String vetPhone;				// string
    String  vetAddress;	// string

    public Vet() {}
    public Vet(String vetName, String vetPhone, String vetAddress) {
        this.vetName = vetName;
        this.vetPhone = vetPhone;
        this.vetAddress = vetAddress;
    }

    public Vet(String _id, String vetName, String vetPhone, String vetAddress) {
        this._id = _id;
        this.vetName = vetName;
        this.vetPhone = vetPhone;
        this.vetAddress = vetAddress;
    }
}
