package com.example.barut.petmatch;

/**
 * Created by barut on 28.05.2017.
 */
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barut.petmatch.dataobject.Pet;
import com.example.barut.petmatch.request.GetPetFullRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import static com.example.barut.petmatch.Dashboard.MyPREFERENCES;


public class LayoutOne extends Fragment implements OnRefreshListener<ScrollView>,AsyncListener {

    Context context;
    TextView petname;
    TextView age;
    TextView cins;
    TextView cityy;
    BrandTextView pendingAdoption;
    BrandTextView pendingMatch;
    TextView cinsiyet;
    TextView kısırlık;




    public static Fragment newInstance(Context context) {
        LayoutOne f = new LayoutOne();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.layout_one, null);
        petname=(TextView)root.findViewById(R.id.petname);
        cityy=(TextView)root.findViewById(R.id.cityy);
        age=(TextView)root.findViewById(R.id.age);
        cins=(TextView)root.findViewById(R.id.cins);
        cinsiyet=(TextView)root.findViewById(R.id.cinsiyet);
        kısırlık=(TextView)root.findViewById(R.id.kısırlık);
        pendingAdoption=(BrandTextView)root.findViewById(R.id.pendingAdoption);
        pendingMatch=(BrandTextView)root.findViewById(R.id.pendingMatch);
        SharedPreferences pref= getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        pendingMatch.setText("pendingMatch");
        pendingAdoption.setText("pendingAdoption");
        if(Utils.ownAccount) {
            petname.setText(pref.getString("PetName", ""));
            cityy.setText(pref.getString("PetCity", ""));
            int ag = pref.getInt("PetAge", 1);
            String agee = Integer.toString(pref.getInt("PetAge", 1));
            age.setText(agee);
            cins.setText(pref.getString("cinsadı", ""));
            if (pref.getInt("PetGender", 2) == 1)
                cinsiyet.setText("Erkek");
            else
                cinsiyet.setText("Kız");
            if (pref.getBoolean("isSterilized", false))
                kısırlık.setText("Kısır");
            else
                kısırlık.setText("Kısır Değil");
            if (pref.getBoolean("pendingMatch", false)) {
                pendingMatch.setTextColor(Color.rgb(250, 128, 104));

            } else {
                pendingMatch.setTextColor((Color.BLACK));
            }

            if (pref.getBoolean("pendingAdoption", false)) {
                pendingAdoption.setTextColor(Color.rgb(250, 128, 104));

            } else {
                pendingAdoption.setTextColor((Color.BLACK));
            }
        }
        else   if(Utils.fromMyMatches) {
            new GetPetFullRequest(Utils.petID,LayoutOne.this);
        }

        else if(!Utils.ownAccount){
            petname.setText(Utils.petname);
            cityy.setText(Utils.petCity);
            int ag = Utils.petAge;
            String agee = Integer.toString(ag);
            age.setText(agee);
            cins.setText(Utils.petType);
            if (Utils.petGender == 1)
                cinsiyet.setText("Erkek");
            else
                cinsiyet.setText("Kız");
            if (Utils.isSterilized)
                kısırlık.setText("Kısır");
            else
                kısırlık.setText("Kısır Değil");
            if (Utils.pendingMatch) {
                pendingMatch.setTextColor(Color.rgb(250, 128, 104));

            } else {
                pendingMatch.setTextColor((Color.BLACK));
            }

            if (Utils.pendingAdoption) {
                pendingAdoption.setTextColor(Color.rgb(250, 128, 104));

            } else {
                pendingAdoption.setTextColor((Color.BLACK));
            }

        }



        return root;
    }

    @Override
    public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {


    }

    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {
        JSONObject object=(JSONObject)obj;
        try {
            if(object.getString("response")=="1")
            {
                Pet pet=new Pet(object);
                petname.setText(Utils.petname);
                cityy.setText(Utils.petCity);
                int ag = Utils.petAge;
                String agee = Integer.toString(ag);
                age.setText(agee);
                cins.setText(Utils.petType);
                if (Utils.petGender == 1)
                    cinsiyet.setText("Erkek");
                else
                    cinsiyet.setText("Kız");
                if (Utils.isSterilized)
                    kısırlık.setText("Kısır");
                else
                    kısırlık.setText("Kısır Değil");
                if (Utils.pendingMatch) {
                    pendingMatch.setTextColor(Color.rgb(250, 128, 104));

                } else {
                    pendingMatch.setTextColor((Color.BLACK));
                }

                if (Utils.pendingAdoption) {
                    pendingAdoption.setTextColor(Color.rgb(250, 128, 104));

                } else {
                    pendingAdoption.setTextColor((Color.BLACK));
                }
            }
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        // TODO Auto-generated method stub

    }

}
