package com.example.barut.petmatch;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barut.petmatch.dataobject.Pet;
import com.example.barut.petmatch.request.GetPetFullRequest;
import com.example.barut.petmatch.request.GetUserPetsRequest;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

import com.example.barut.petmatch.dataobject.DashboardPhotoVO;
import com.example.barut.petmatch.request.DashboardPhotosRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Dashboard extends BaseFragmentActivity implements AsyncListener {

    private LinearLayout layout1;
    private LinearLayout layout2;
    private int leftTemp;
    private int rightTemp;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences pref;
    private RelativeLayout  isnotLoginLayout;
    private	PullToRefreshScrollView mPullRefreshScrollView;
    private ScrollView mScrollView;
    int currentpage=1;
    int photoItemCount=10;
    private ArrayList<DashboardPhotoVO> arrayListPhoto=new ArrayList<DashboardPhotoVO>();
    public static ArrayList<Pet> petArrayList=new ArrayList<>();

    @Override
    public void onBackPressed() {
        Log.i("BACK","back pressed girdi");
        super.onBackPressed();
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tvw = (View)inflater.inflate(R.layout.activity_dashboard, null);
        layout1=(LinearLayout)tvw.findViewById(R.id.linear_bir);
        layout2=(LinearLayout)tvw.findViewById(R.id.linear_iki);
        isnotLoginLayout=(RelativeLayout)tvw.findViewById(R.id.isnotlogin_layout);
        getContentHolder().addView(tvw);
        header_logo_layout.setVisibility(View.VISIBLE);
        pref=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        if(pref.getBoolean("isLogin", false)) {
            Utils.isLogin = true;
            Utils.token=pref.getString("token","");
            Utils.username=pref.getString("name", "");
            Utils.userId=pref.getString("userId", "");
            headerLayout.setClickable(true);
            headerMenu.setVisibility(View.VISIBLE);
            setHeaderText("");
            headerText.setVisibility(View.GONE);
            isnotLoginLayout.setVisibility(View.GONE);
            setSlidingMenu();
            System.out.println("if is login truda  ");
            overFlowButton.setVisibility(View.VISIBLE);
            new GetUserPetsRequest(Utils.token,Dashboard.this);
        }
        else {
            Utils.isLogin=false;
            setHeaderText("Giriş");
            headerLayout.setClickable(false);
            headerMenu.setVisibility(View.GONE);
            System.out.println("else is login");
        }

        isnotLoginLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final MaterialDialog materialDialog = new MaterialDialog(Dashboard.this);
                materialDialog.setMessage("Lütfen Giriş Yapınız")
                        .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();
                                Intent intent=new Intent(getApplicationContext(), SignInActivity.class);
                                startActivity(intent);
                            }
                        });
                materialDialog.show();

            }});
        headerText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), SignInActivity.class);
                startActivity(intent);
            }
        });


        overFlowButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(getApplicationContext(), v);


                popupMenu.inflate(R.menu.pet_menu);
                if(petArrayList.size()==0) {
                    Intent i=new Intent(getApplicationContext(),AddAPetActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                for(int i=0;i < petArrayList.size();i++) {
                    popupMenu.getMenu().getItem(i+1).setVisible(true);
                    popupMenu.getMenu().getItem(i+1).setTitle(petArrayList.get(i).petName);
                    popupMenu.getMenu().getItem(i+1).setIcon(petArrayList.get(i).petProfileDrawableID);

                }

                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.addpet:
                                if(Utils.petNum<3) {
                                    Intent i = new Intent(getApplicationContext(), AddAPetActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                }
                                else {
                                    final MaterialDialog materialDialog = new MaterialDialog(Dashboard.this);
                                    materialDialog.setMessage("Maksimum hayvan sahip olma kotanıza ulaştınız.")
                                            .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    materialDialog.dismiss();

                                                }
                                            });
                                    materialDialog.show();
                                }
                                return true;
                            case R.id.pet1:
                                new GetPetFullRequest(petArrayList.get(0).id,Dashboard.this);
                                return true;
                            case R.id.pet2:
                                new GetPetFullRequest(petArrayList.get(1).id,Dashboard.this);
                                return true;

                            case R.id.pet3:
                                new GetPetFullRequest(petArrayList.get(2).id,Dashboard.this);
                                return true;

                            default:
                                return false;
                        }

                    }
                });

            }
        });
        Intent intent=getIntent();
        String toast=intent.getStringExtra("toast");
        if(toast!=null)
        {
            Toast.makeText(Dashboard.this,toast,Toast.LENGTH_SHORT).show();
        }
        openProgress();
        mPullRefreshScrollView = (PullToRefreshScrollView) findViewById(R.id.pull_refresh_scrollview);
        mPullRefreshScrollView.setOnRefreshListener(new OnRefreshListener<ScrollView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
                arrayListPhoto.removeAll(arrayListPhoto);
                currentpage++;
                new DashboardPhotosRequest(Dashboard.this,String.valueOf(currentpage),String.valueOf(photoItemCount));
            }
        });
        mScrollView = mPullRefreshScrollView.getRefreshableView();

        new DashboardPhotosRequest(Dashboard.this,String.valueOf(currentpage),String.valueOf(photoItemCount));
    }

    public void run(){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("petID",Utils.petID);
        editor.putBoolean("isSterilized",Utils.isSterilized);
        editor.putString("PetName", Utils.petname);
        editor.putString("cinsadı", Utils.petType);
        editor.putBoolean("pendingAdoption",Utils.pendingAdoption);
        editor.putBoolean("pendingMatch",Utils.pendingMatch);
        if(Utils.pendingMatch) {
            editor.putString("prefType1", Utils.preff1);
            editor.putString("prefType2", Utils.preff2);
            editor.putString("prefType3", Utils.preff3);
        }
        editor.putInt("PetAge",Utils.petAge);
        editor.putString("PetCity",Utils.petCity);
        editor.putString("PetProfilePhotoPath", Utils.petprofilephoto);
        editor.putString("PetCoverPhotoPath", Utils.petcoverphto);
        editor.putString("VetEmail", Utils.vetMail);
        editor.putString("VetMobilePhone", Utils.vetPhone);;
        editor.putInt("PetGender", Utils.petGender);
        editor.putInt("PetDrawablePath",R.drawable.bulldog);
        editor.commit();
    }
    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {
        if(queue==RequestsQueue.GET_USER_PETS) {
            System.out.println("LOG: GET_USER_PETS");
            Log.i("PET","asyn");
            JSONObject object=(JSONObject)obj;
            try {
                if(object.getString("response").equals("1"))
                {
                    petArrayList.removeAll(petArrayList);
                    JSONArray arr=object.getJSONArray("list");
                    for(int i=0;i<arr.length();i++) {
                        String name=arr.getJSONObject(i).getString("petName");
                        String photopath=arr.getJSONObject(i).getString("petProfilePhotoPath");
                        String id=arr.getJSONObject(i).getString("petID");
                        Pet pet=new Pet(name,photopath,id);
                        petArrayList.add(pet);
                    }
                    Utils.petNum=petArrayList.size();

                    if(Utils.petNum==0) {
                        //overFlowButton.setImageResource(R.drawable.add);
                        //Utils.petname="";
                        overFlowButton.setImageResource(android.R.drawable.ic_input_add);
                    }
                    else {
                        overFlowButton.setImageResource(R.drawable.action_button);
                        if(Utils.justDeletedPet) {
                            new GetPetFullRequest(petArrayList.get(0).id,Dashboard.this);

                        }

                    }
                }

            } catch(JSONException j) {
                j.printStackTrace();
            }
        }
        if (queue==RequestsQueue.GET_PET_FULL) {
            JSONObject object=(JSONObject)obj;
            try {
                if(object.getString("response")=="1")
                {
                    Pet pet=new Pet(object);
                    run();
                    String message="Switched to your pet: "+Utils.petname+"\nPet Profile updated";
                    Log.i("DEVPET",message);
                    Toast.makeText(Dashboard.this,message, Toast.LENGTH_SHORT).show();
                }
                else {
                    String message="A problem occurred.";
                    Log.i("DEVPET",message);
                    Toast.makeText(Dashboard.this,message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException j) {
                j.printStackTrace();
            }

        }
        if(queue==RequestsQueue.GET_DASHBOARD) {
            JSONArray array;
            array = (JSONArray) obj;
            for (int i = 0; i < array.length(); i++) {

                try {
                    DashboardPhotoVO photoVO = new DashboardPhotoVO(array.getJSONObject(i));
                    arrayListPhoto.add(photoVO);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            if (array.length() == 0) {
                final MaterialDialog materialDialog = new MaterialDialog(Dashboard.this);
                materialDialog.setMessage("Görebileceğiniz fotoğraflar şimdilik bu kadar.")
                        .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();

                            }
                        });
                materialDialog.show();
            }

            uploadPhotosToScreen();

            closeProgress();
            mPullRefreshScrollView.onRefreshComplete();
            System.out.println("LOG2: GET_USER_PETS");

        }

    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        // TODO Auto-generated method stub

    }
    public void uploadPhotosToScreen() {
        //Utils.layoutwidth=layout1.getWidth();
        // DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        //float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        //float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        //Utils.layoutwidth=(int)dpWidth;

        Utils.layoutwidth=720;
        Log.i("CIV","Linearwidth: "+Utils.layoutwidth);
        for (int i = 0; i < arrayListPhoto.size(); i++) {

            if(i%2==0){
                if(leftTemp<=rightTemp){
                    leftTemp=leftTemp+Integer.parseInt(arrayListPhoto.get(i).height);
                    Log.i("DEV","dashboardda customimageview'ı layout'a ekliycek. ImagePath: "+arrayListPhoto.get(i).path);
                    layout1.addView(CustomImageview.getObject(this, arrayListPhoto.get(i).path,arrayListPhoto.get(i).Id,Integer.parseInt(arrayListPhoto.get(i).width),Integer.parseInt(arrayListPhoto.get(i).height),Utils.layoutwidth));
                }
                else{

                    rightTemp=rightTemp+Integer.parseInt(arrayListPhoto.get(i).height);
                    Log.i("DEV","dashboardda customimageview'ı layout'a ekliycek. ImagePath: "+arrayListPhoto.get(i).path);
                    layout2.addView(CustomImageview.getObject(this, arrayListPhoto.get(i).path,arrayListPhoto.get(i).Id,Integer.parseInt(arrayListPhoto.get(i).width),Integer.parseInt(arrayListPhoto.get(i).height),Utils.layoutwidth));
                }
            }
            else{

                if(rightTemp<=leftTemp){
                    rightTemp=rightTemp+Integer.parseInt(arrayListPhoto.get(i).height);
                    Log.i("DEV","dashboardda 2. customimageview'ı layout'a ekliycek. ImagePath: "+arrayListPhoto.get(i).path);
                    layout2.addView(CustomImageview.getObject(this, arrayListPhoto.get(i).path,arrayListPhoto.get(i).Id,Integer.parseInt(arrayListPhoto.get(i).width),Integer.parseInt(arrayListPhoto.get(i).height),Utils.layoutwidth));
                }
                else{

                    leftTemp=leftTemp+Integer.parseInt(arrayListPhoto.get(i).height);
                    Log.i("DEV","dashboardda 2. customimageview'ı layout'a ekliycek. ImagePath: "+arrayListPhoto.get(i).path);
                    layout1.addView(CustomImageview.getObject(this, arrayListPhoto.get(i).path,arrayListPhoto.get(i).Id,Integer.parseInt(arrayListPhoto.get(i).width),Integer.parseInt(arrayListPhoto.get(i).height),Utils.layoutwidth));

                }
            }
        }
    }
}
