package com.example.barut.petmatch;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class BrandTextView extends TextView {
	public BrandTextView(final Context context) {
		super(context);
		setAntiAlliasSubPixel();
		
	}


	public BrandTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setAntiAlliasSubPixel();
	}
	public BrandTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setAntiAlliasSubPixel();
	}


	public void setTypeface(Typeface tf, int style) {
		if(!isInEditMode()){
			// Your custom code that is not letting the Visual Editor draw properly
			// i.e. thread spawning or other things in the constructor
			if (style == Typeface.BOLD) {
				super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf"));

			}else if(style == Typeface.NORMAL){
				super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf"));

			} else {
				super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf"));
			}

		}
	}

	public void setAntiAlliasSubPixel(){
		this.getPaint().setAntiAlias(true);
		this.getPaint().setSubpixelText(true);

	}
	


}
