package com.example.barut.petmatch.request;

import android.os.AsyncTask;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by barut on 02.06.2017.
 */

public class UpdatePetRequest extends AsyncTask<Void, Void, String> {
    private String token="";
    AsyncListener asc;
    private String petName="";
    private String petType="";
    private String petCity="";
    private int petGender;
    private int petAge;
    private boolean isSterilized=false;
    // private ArrayList<String>   prefType=new ArrayList<String>();
    private  String vetMail="";
    private String vetPhone="";
    boolean pendingAdoption;
    boolean pendingMatch;
    String prefType1;
    String prefType2;
    String prefType3;
    String petID;
    public UpdatePetRequest(String petID,boolean pendingAdoption, boolean pendingMatch,String prefType1,String prefType2,String prefType3, String token, String petName, String petType, String petCity, int petGender,
                            int petAge, boolean isSterilized, String vetMail,String vetPhone, AsyncListener asc) {
        this.token = token;
        this.asc=asc;
        this.petName=petName;
        this.petType=petType;
        this.petCity=petCity;
        this.petGender=petGender;
        this.petAge=petAge;
        this.isSterilized=isSterilized;
        this.vetMail=vetMail;
        this.vetPhone=vetPhone;
        this.pendingAdoption=pendingAdoption;
        this.pendingMatch=pendingMatch;
        this.prefType1=prefType1;
        this.prefType2=prefType2;
        this.prefType3=prefType3;
        this.petID=petID;
        execute();
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("token",token);
            jsonObject.put("petID",petID);
            jsonObject.put("petName",petName);
            jsonObject.put("petType",petType);
            jsonObject.put("petGender",petGender);
            jsonObject.put("petAge",petAge);
            jsonObject.put("isSterilized",isSterilized);
            jsonObject.put("petCity",petCity);
            jsonObject.put("vetMail",vetMail);
            jsonObject.put("vetPhone",vetPhone);
            jsonObject.put("pendingAdoption",pendingAdoption);
            jsonObject.put("pendingMatch",pendingMatch);
            jsonObject.put("prefType1",prefType1);
            jsonObject.put("prefType2",prefType2);
            jsonObject.put("prefType3",prefType3);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_PETS + "updatePet", jsonObject);

    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.UPDATE_PET, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.UPDATE_PET);
        }
        super.onPostExecute(result);
    }
}

