package com.example.barut.petmatch;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


import com.example.barut.petmatch.dataobject.UserVO;
import com.example.barut.petmatch.request.LoginRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;


import org.json.JSONException;
import org.json.JSONObject;

public class Login extends BaseFragmentActivity implements AsyncListener {


	private EditText editTextEmail;
	private EditText editTextPassword;
	String userName;
	Button button;
	Button autoComplete;
	SharedPreferences pref;
	public static final String MyPREFERENCES = "MyPrefs";
	String password;
	View tvw;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.login_layout);
		//LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//tvw = (View) inflater.inflate(R.layout.login_layout, null);
		button = (Button) findViewById(R.id.angry_btn);
		editTextEmail = (EditText) findViewById(R.id.edittext_email);
		editTextPassword = (EditText) findViewById(R.id.edittext_password);
		autoComplete=(Button)findViewById(R.id.auto_complete);
		//getContentHolder().addView(tvw);
		headerLayout.setClickable(false);
		headerMenu.setVisibility(View.GONE);
		pref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

button.setOnClickListener(new OnClickListener() {
	@Override
	public void onClick(View v) {
new LoginRequest(editTextEmail.getText().toString(),editTextPassword.getText().toString(),Login.this);
		/*Intent intent = new Intent(getApplicationContext(), Dashboard.class);
		Utils.isLogin=true;
		Utils.name="Berkin";
		Utils.surname="Barut";
		run();
		startActivity(intent);*/

	}
});
autoComplete.setOnClickListener(new OnClickListener() {
	@Override
	public void onClick(View v) {
		editTextEmail.setText("barut@sabanciuniv.edu");
		editTextPassword.setText("1234567b");
	}
});
	}




	@Override
	public void asyncOperationSucceded(Object obj, RequestsQueue queue,
									   String extraData) {
		// TODO Auto-generated method stub


		JSONObject jsonobj;
		jsonobj = (JSONObject) obj;
		try {
			if (jsonobj.getString("response") == "1") {
				UserVO user = new UserVO(jsonobj);
				Utils.isLogin = true;
				run();
				Intent intent = new Intent(getApplicationContext(), Dashboard.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
				//intent.putExtra("petname1",jsonobj.)
				startActivity(intent);
			}
			/*else if(jsonobj.getString("response") == "0") {
				final MaterialDialog materialDialog = new MaterialDialog(Login.this);
				materialDialog.setMessage("Şifreniz hatalıdır." +
						"Lütfen tekrar deneyiniz.")
						.setPositiveButton(android.R.string.yes, new OnClickListener() {
							@Override
							public void onClick(View v) {
								materialDialog.dismiss();
							}
						});
				materialDialog.show();
			} */
			else {
				final MaterialDialog materialDialog = new MaterialDialog(Login.this);
				materialDialog.setMessage("Şifreniz veya kullanıcı adınız hatalıdır." +
						"Lütfen tekrar deneyiniz.")
						.setPositiveButton(android.R.string.yes, new OnClickListener() {
							@Override
							public void onClick(View v) {
								materialDialog.dismiss();
							}
						});
				materialDialog.show();
			}
		}
		catch(JSONException j){
			j.printStackTrace();
		}





	}

	@Override
	public void asyncOperationFailed(Object obj, RequestsQueue queue) {
		final MaterialDialog materialDialog = new MaterialDialog(Login.this);
		materialDialog.setMessage("Şifreniz veya kullanıcı adınız hatalıdır." +
				"Lütfen tekrar deneyiniz.")
				.setPositiveButton(android.R.string.yes, new OnClickListener() {
					@Override
					public void onClick(View v) {
						materialDialog.dismiss();
					}
				});
		materialDialog.show();
	}

	public void run() {
		Editor editor = pref.edit();
		editor.putBoolean("isLogin", Utils.isLogin);
		editor.putString("token",Utils.token);
		editor.putString("name", Utils.name);
		editor.putString("surname", Utils.surname);
		editor.putString("userId", Utils.userId);
		editor.putString("UserName", Utils.username);
		editor.putString("ProfilePhotoPath", Utils.profilephoto);
		editor.putString("CoverPhotoPath", Utils.coverphto);
		editor.putString("Email", Utils.eposta);
		editor.putString("MobilePhone", Utils.phone);
		editor.putString("city", Utils.city);
		editor.commit();
	}

}