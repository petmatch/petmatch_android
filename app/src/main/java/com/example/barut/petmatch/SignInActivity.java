package com.example.barut.petmatch;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.gc.materialdesign.views.ButtonFlat;

public class SignInActivity extends BaseFragmentActivity{

	ButtonFlat kaydol;
	ButtonFlat giriş;
	ImageView removeButton;
	View tvw;

@SuppressLint("InflateParams")
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		tvw = (View)inflater.inflate(R.layout.sign_in_layout, null);
		giriş=(ButtonFlat) tvw.findViewById(R.id.buttonflat_giris);
		giriş.setBackgroundColor(Color.rgb(253, 127, 106));
		kaydol=(ButtonFlat)tvw.findViewById(R.id.buttonflat_kaydol);
		kaydol.setBackgroundColor(Color.rgb(253, 127, 106));
		removeButton=(ImageView)tvw.findViewById(R.id.remove_button);

getContentHolder().addView(tvw);

	headerLayout.setClickable(false);
	headerMenu.setVisibility(View.GONE);
removeButton.setOnClickListener(new OnClickListener() {
	@Override
	public void onClick(View v) {
		finish();
	}
});
		kaydol.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(getApplicationContext(), SignUpPage.class);
				startActivity(intent);
				
			}
		});
		giriş.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(getApplicationContext(), Login.class);
				startActivity(intent);
				
			}
		});

	}






}
