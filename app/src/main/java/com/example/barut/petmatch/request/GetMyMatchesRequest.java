package com.example.barut.petmatch.request;

import android.os.AsyncTask;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lENOVO on 02.06.2017.
 */

public class GetMyMatchesRequest extends AsyncTask<Void, Void, String> {

    private AsyncListener asc;
    public GetMyMatchesRequest(AsyncListener asc) {
        super();
        try {
            this.asc=asc;
            execute();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
        try {


            jsonObject.put("token", Utils.token);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_USERS + "getMyMatches", jsonObject);
    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.GET_MYMATCHES, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.GET_MYMATCHES);
        }
        super.onPostExecute(result);
    }
}
