package com.example.barut.petmatch.request;

import android.os.AsyncTask;
import android.util.Log;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by barut on 22.03.2017.
 */
public class RegisterRequest extends AsyncTask<Void, Void, String>
    {
        String username;					// string
        String ownerName;			// string
        String email;		// string
        String phone;
        String surname;
        String password; // string
        int gender;
        String city;
        AsyncListener asc;
//username,name, surname, password.getText().toString(), email, phone,gender ,SignUpPagePhotoUpload.this
        public RegisterRequest(String city,String username, String ownerName, String email,String phone, String surname, String password, int gender, AsyncListener asc) {
            this.city=city;
            this.username=username;
            this.ownerName = ownerName;
            this.email = email;
            this.phone=phone;
            this.surname=surname;
            this.password=password;
            this.gender=gender;
            this.asc = asc;
            execute();
        }

        @Override
        protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
            System.out.println("LOG: doinbackground register girdi");
        try {
            jsonObject.put("username",username);
            jsonObject.put("ownerName", ownerName);
            jsonObject.put("eMail", email);
            jsonObject.put("ownerSurname", surname);
            jsonObject.put("password", password);
            jsonObject.put("phone", phone);
            jsonObject.put("ownerGender", gender);
            jsonObject.put("city", city);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

              String returned= ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_USERS + "register", jsonObject);
            System.out.println("LOG: "+returned);
            Log.i("REGISTER",returned);
            return returned;
    }

        @Override
        protected void onPostExecute(String result) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.REGISTER, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.REGISTER);
        }
        super.onPostExecute(result);
    }
}
