package com.example.barut.petmatch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.barut.petmatch.StorageStub.UserStub;
import com.example.barut.petmatch.request.RegisterRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;
//import com.example.barut.request.PostUserRequest;
//import com.example.barut.request.UploadUserPhotoOrCoverRequest;


import eu.janmuller.android.simplecropimage.CropImage;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class SignUpPagePhotoUpload extends BaseFragmentActivity implements AsyncListener {

    String Biography;
    String userName;
    String password2;
    private EditText password;
    private EditText passwordConfirm;
    private BrandTextView textViewNext;
    public static final String TAG = "SignUpPagePhotoUpload";
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final String TEMP_PHOTO_FILE_NAME_2 = "temp2_photo.jpg";
    private CircularImageView profileImage;
    private ImageView coverPhooto;
    private ImageView backButton;
    private Button autoComplete;
    private static int PICK_FROM_GALLERY = 1;
    private static int	PICK_FROM_CAMERA= 2;
    private static int COVER_FROM_GALLERY = 3;
    private static int	COVER_FROM_CAMERA= 4;
    private static int	SELECT_PHOTO_2= 1000;
    private static int	SELECT_PHOTO= 1001;
    public static final int REQUEST_CODE_GALLERY      = 0x9;
    public static final int REQUEST_CODE_GALLERY_COVER = 0x12;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x10;
    public static final int REQUEST_CODE_CROP_IMAGE   = 0x11;
    public static final int REQUEST_CODE_CROP_IMAGE_COVER=0x8;
    private File      mFileTemp;
    Bitmap icon;
    Bitmap icon2;
    private String name="";
    private String surname="";
    private String fullname="";
    private String username="";
    private String phone="";
    private String email="";
    private Integer gender=2;
    //private String userID="";
    Bitmap photo;
    Bitmap coverPhoto;
    boolean isCover;
    SharedPreferences pref;
    public static final String MyPREFERENCES = "MyPrefs" ;
    String filename = "profile.png";
    String filename2 = "cover.png";
    String city="";

    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        pref=this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        setContentView(R.layout.activity_sign_up_page_photo_upload);
        actionBar=getActionBar();
        actionBar.hide();
        name=getIntent().getStringExtra("name");
        surname=getIntent().getStringExtra("lastname");
        phone=getIntent().getStringExtra("phone");
        email=getIntent().getStringExtra("email");
        username=getIntent().getStringExtra("username");
        gender=getIntent().getIntExtra("gender",2);
        city=getIntent().getStringExtra("city");

        fullname=name + " " +  surname;
        Utils.city=city;
        Utils.name=name;
        Utils.surname=surname;
        Utils.phone=phone;
        Utils.eposta=email;
        Utils.username=username;
        backButton = (ImageView)findViewById(R.id.back_button_sign_up);
        password=(EditText)findViewById(R.id.edittext_password);
        passwordConfirm=(EditText)findViewById(R.id.edittext_password_again);
        textViewNext=(BrandTextView)findViewById(R.id.text_next);
        profileImage=(CircularImageView)findViewById(R.id.profile_icon);
        autoComplete=(Button)findViewById(R.id.autoComplete);
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        }
        else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
        coverPhooto = (ImageView)findViewById(R.id.cover);
        autoComplete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                password.setText("1234567b");
                passwordConfirm.setText("1234567b");
            }
        });
        backButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        coverPhooto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery2();


            }
        });
        profileImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                openGallery();


            }
        });
        textViewNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String validate=validateNewPass(password.getText().toString(), passwordConfirm.getText().toString());
                if(!validate.equals("Başarılı")) {
                    final MaterialDialog materialDialog = new MaterialDialog(SignUpPagePhotoUpload.this);
                    materialDialog.setMessage(validate)
                            .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    materialDialog.dismiss();

                                }
                            });
                    materialDialog.show();
                }
                else {


                    Intent i=new Intent(getApplicationContext(), PrivacyPolicyActivity.class);

                    startActivityForResult(i, 12);
                }
            }
        });
    }

    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {
        //System.out.println(obj);

        if(queue==RequestsQueue.REGISTER) {
            JSONObject object=(JSONObject)obj;
            try {

               /* Utils.userId = object.getString("Id");
                Utils.username = object.getString("UserName");
                //Utils.profilephoto = object.getString("ProfilePhotoPath");
                //Utils.coverphto = object.getString("CoverPhotoPath");
                Utils.eposta = object.getString("Email");
                Utils.phone = object.getString("MobilePhone");
                Utils.surname = object.getString("LastName");
                userName = object.getString("FullName");
                Utils.name = object.getString("Name");
                password2 = object.getString("Password");
                Utils.biography = object.getString("Biography"); */
               Log.i("REGISTER","Register response: "+object.getString("response"));
                int i=0;
               if(object.getString("response").equals("1")) {
                   startServiceForUploadUserPhotoOrCover(Utils.profile, filename, false);
                   startServiceForUploadUserPhotoOrCover(Utils.cover, filename2, true);
                   Utils.isLogin=true;
                   Utils.token=object.getString("token");
                   run();
                   Intent intent = new Intent(SignUpPagePhotoUpload.this, Dashboard.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                   closeProgress();
                   startActivity(intent);
               }
               else if(object.getString("response")=="-1") {
                   String toast="The user already exists!";
                   Utils.isLogin=false;
                   run();
                   Intent intent = new Intent(SignUpPagePhotoUpload.this, Dashboard.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                   intent.putExtra("toast",toast);
                   closeProgress();
                   startActivity(intent);

               } else if(object.getString("response")=="0") {
                   String toast="A problem occured on PetMatch Server. Couldn't register.";
                   Utils.isLogin=false;
                   run();
                   Intent intent = new Intent(SignUpPagePhotoUpload.this, Dashboard.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                   intent.putExtra("toast",toast);
                   closeProgress();
                   startActivity(intent);
               }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
       /* if(queue==RequestsQueue.REGISTER_PHOTO) {
            Utils.isLogin = true;
            JSONObject object=(JSONObject)obj;
            try {
                if(extraData.contentEquals("false")){
                    System.out.println("asyncsucceeded of profile photo.");
                    Utils.profilephoto=object.toString();
                    startServiceForUploadUserPhotoOrCover(Utils.cover,filename2,true);
                }
                if(extraData.equals("true")) {
                    Utils.coverphto=object.toString();

                    System.out.println("Will move to the dashboardnew activity now.");
                    run();
                    Intent intent=new Intent(SignUpPagePhotoUpload.this,Dashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent); }

            } catch (Exception a) {
                // TODO Auto-generated catch block
                a.printStackTrace();
            }
        } */

    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap;


        if(requestCode==REQUEST_CODE_CROP_IMAGE){
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }

            bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
            profileImage.setImageBitmap(bitmap);
            //photo=bitmap;
            Utils.profile=bitmap;
            //isCover=false;
        }
        if(requestCode==REQUEST_CODE_CROP_IMAGE_COVER){
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }

            bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
            coverPhooto.setImageBitmap(bitmap);
            //coverPhoto=bitmap;
            Utils.cover=bitmap;
            //isCover=true;
            coverPhooto.setBackgroundResource(R.drawable.image_shape);
        }
        if(requestCode==REQUEST_CODE_GALLERY_COVER){
            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage2();

            } catch (Exception e) {

                Log.e(TAG, "Error while creating temp file", e);
            }
        }
        if(requestCode==REQUEST_CODE_GALLERY){
            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage();

            } catch (Exception e) {

                Log.e(TAG, "Error while creating temp file", e);
            }
        }

        if(requestCode==SELECT_PHOTO){
            if(resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImage);
                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    profileImage.setImageBitmap(yourSelectedImage);
                    Utils.profile=yourSelectedImage;
                    //isCover=false;

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }

        if(requestCode==SELECT_PHOTO_2){
            if(resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImage);
                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    coverPhooto.setImageBitmap(yourSelectedImage);
                    Utils.cover=yourSelectedImage;
                    coverPhooto.setBackgroundResource(R.drawable.rectangle);
                    //isCover=true;

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }
        if (requestCode == 12) {
            if(resultCode == RESULT_OK){
                String result=data.getStringExtra("result");

                if(result.equalsIgnoreCase("ok")){
                    System.out.println("Will post user now");
              /*
                    new UserStub();
                    startServiceForUploadUserPhotoOrCover(Utils.profile,filename,false);
                    startServiceForUploadUserPhotoOrCover(Utils.cover,filename2,true);
                    run();
                    System.out.println("Will move to the dashboardnew activity now.");
                    Intent intent=new Intent(SignUpPagePhotoUpload.this,Dashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);   */
              openProgress();
                    new RegisterRequest(city,username, name,email, phone, surname, password.getText().toString(),gender ,SignUpPagePhotoUpload.this);
                    //System.out.println("Continue main thread while post user working.");
                    //run();
                }

            }
        }
    }

    private void startServiceForUploadUserPhotoOrCover(Bitmap bitmap,String fileName,boolean isCover) {
        String path;FileOutputStream out=null;
        path = Environment.getExternalStorageDirectory().toString();
        File imageFile = new File(path,fileName);
        try {
            out = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        if(profileImage.getDrawable()!=null&&!isCover){
            System.out.println("startServiceForUploadPhoto: Profile");
            Utils.profilephoto=imageFile.toString();
           // new UploadUserPhotoOrCoverRequest(Utils.userId, isCover, imageFile, SignUpPagePhotoUpload.this);
            Log.i("PHOTO","Profile photo path: "+Utils.profilephoto);
        }
        if(coverPhooto.getDrawable()!=null&&isCover){
            Utils.coverphto=imageFile.toString();
            System.out.println("startServiceForUploadPhoto: Cover");
         //   new UploadUserPhotoOrCoverRequest(Utils.userId, isCover, imageFile, SignUpPagePhotoUpload.this);
            Log.i("PHOTO","Cover photo path: "+Utils.coverphto);
        }

        //run();
    }

    public void run(){

        Editor editor = pref.edit();
        editor.putBoolean("isLogin", true);
        editor.putString("token",Utils.token);
        editor.putString("name", Utils.name);
        editor.putString("surname", Utils.surname);
        editor.putString("userId", Utils.userId);
        editor.putString("UserName", Utils.username);
        editor.putString("ProfilePhotoPath", Utils.profilephoto);
        editor.putString("CoverPhotoPath", Utils.coverphto);
        editor.putString("Email", Utils.eposta);
        editor.putString("city", Utils.city);
        editor.putString("MobilePhone", Utils.phone);
        //editor.putString("password", password2);
        //editor.putString("Biography", Utils.biography);
        editor.putInt("Gender", gender);
        editor.commit();

    }
    public String validateNewPass(String pass1, String pass2){
        StringBuilder retVal = new StringBuilder();

        if(pass1.length() < 1 || pass2.length() < 1 )retVal.append("boş alanlar var");

        if (pass1 != null && pass2 != null) {

            if (pass1.equals(pass2)) {

                pass1 = pass2;
                boolean hasUppercase = !pass1.equals(pass1.toString());
                boolean hasLowercase = !pass1.equals(pass1.toUpperCase());
                boolean hasNumber = pass1.matches(".*\\d.*");
                boolean noSpecialChar = pass1.matches("[a-zA-Z0-9 ]*");

                if (pass1.length() < 6) {
                    retVal.append("Şifre en az  6 karakter olmalıdır");
                }

                if (!hasUppercase&&!hasLowercase) {

                    retVal.append("Şifre harf gerektirir.");
                }



                if (!hasNumber) {

                    retVal.append("Şifre numara gerektirir ");
                }


            }else{

                retVal.append("Şifre eşleşmiyor");
            }
        }else{

            retVal.append("Şifre boş<br>");
        }
        if(retVal.length() == 0){

            retVal.append("Başarılı");
            //   new UpdatePassword(editTextnewpassword.getText().toString(), pref.getString("userId", ""), ChangePasswordActivity.this);




        }

        return retVal.toString();

    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }
    private void openGallery2() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY_COVER);
    }
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
    private void startCropImage() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }
    private void startCropImage2() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE_COVER);
    }
   /* public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }*/

}

