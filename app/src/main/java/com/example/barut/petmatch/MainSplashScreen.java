package com.example.barut.petmatch;

import android.content.Intent;

        import android.os.Bundle;

public class MainSplashScreen extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_splash_screen);
        actionBar=getActionBar();
        actionBar.hide();


        Thread background = new Thread() {
            public void run() {

                try {
                    sleep(5*1000);
                    Intent i=new Intent(getBaseContext(),Dashboard.class);
                    startActivity(i);
                    finish();

                } catch (Exception e) {

                }
            }
        };
        background.start();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}
