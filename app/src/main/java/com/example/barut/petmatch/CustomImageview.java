package com.example.barut.petmatch;

/**
 * Created by barut on 11.05.2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;


import loadimage.ImageLoader;

public class CustomImageview  extends LinearLayout{

    String path;
    ImageView view;
    String id;
    private int  width;
    private int height;
    private int linearwidth;
    private	ImageLoader imgLoader;
    private Context context;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences pref;
    public CustomImageview(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        imgLoader = new ImageLoader(context.getApplicationContext());

    }

    public CustomImageview( Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        imgLoader = new ImageLoader(context.getApplicationContext());
    }
    public static CustomImageview getObject(Context _context, String path,String id,int width,int height,int linearwidth){

        LayoutInflater inflater = (LayoutInflater)_context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        CustomImageview mio;

        mio = (CustomImageview)inflater.inflate(R.layout.custom_imageview, null);
        mio.context = _context;
        mio.path=path;
        mio.id=id;
        mio.width=width;
        mio.height=height;
        mio.linearwidth=linearwidth;

        return mio;
    }
    private Boolean isSettled = false;
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        // TODO Auto-generated method stub
        super.onSizeChanged(w, h, oldw, oldh);
        setupLayout();

        pref=context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


    }

    public void setupLayout(){
        if(!isSettled){
            isSettled = true;
            view=(ImageView)findViewById(R.id.imageview);
            //(int loader = R.drawable.loader;
            //  view.setAdjustViewBounds(true);

            double ratio=(double)linearwidth/(double)width;
            double newHeight=(double)height;
            newHeight=newHeight*ratio;
            view.getLayoutParams().width=linearwidth;
            view.getLayoutParams().height=(int)newHeight;


//view.setImageResource(R.drawable.bckgrnd);
            imgLoader.DisplayImage(path,  view);



        }
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                if(pref.getBoolean("isLogin", false)){
                    Intent intent= new Intent(getContext(), PhotoDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("path", path);
                    intent.putExtra("photoid", id);
                    intent.putExtra("width", width);
                    intent.putExtra("height", height);

                    //					System.out.println(path1);
                    //					System.out.println(id1);

                   // ((Activity)getContext()).startActivityForResult(intent, 10000);
                    getContext().startActivity(intent);

                }

                else{
                    final MaterialDialog materialDialog = new MaterialDialog(getContext());
                    materialDialog.setMessage("Lütfen Giriş Yapınız.")
                            .setNegativeButton("İptal", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    materialDialog.dismiss();

                                }
                            }).setPositiveButton("Giriş",  new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(getContext(), SignInActivity.class);
                            materialDialog.dismiss();
                            getContext().startActivity(intent);

                        }

                    });
                    materialDialog.show();



                }


            }
        });

    }





}
