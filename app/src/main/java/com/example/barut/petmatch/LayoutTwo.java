package com.example.barut.petmatch;

/**
 * Created by barut on 28.05.2017.
 */

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.barut.petmatch.dataobject.Pet;
import com.example.barut.petmatch.request.GetPetFullRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;

import static com.example.barut.petmatch.Dashboard.MyPREFERENCES;


public class LayoutTwo extends Fragment implements AsyncListener {


    TextView fullname;
    TextView city;
    TextView email;
    TextView phone;
    TextView veterinermail;
    TextView veterinernumara;

    int temp;
    public static Fragment newInstance(Context context) {
        LayoutTwo f = new LayoutTwo();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.layout_two, null);
        SharedPreferences pref= getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
       fullname=(TextView)root.findViewById(R.id.fullname);
        city=(TextView)root.findViewById(R.id.city);
        email=(TextView)root.findViewById(R.id.email);
        phone=(TextView)root.findViewById(R.id.phone);
        veterinermail=(TextView)root.findViewById(R.id.veterinermail);
        veterinernumara=(TextView)root.findViewById(R.id.veterinernumara);
        /*
           editor.putString("PetName", Utils.petname);
        editor.putString("cinsadı", Utils.petType);
        editor.putBoolean("pendingAdoption",Utils.pendingAdoption);
        editor.putBoolean("pendingMatch",Utils.pendingMatch);
        if(Utils.pendingMatch) {
            editor.putString("prefType1", Utils.preff1);
            editor.putString("prefType2", Utils.preff2);
            editor.putString("prefType3", Utils.preff3);
        }
        editor.putInt("PetAge",Utils.petAge);
        editor.putString("PetCity",Utils.petCity);
        editor.putString("PetProfilePhotoPath", Utils.petprofilephoto);
        editor.putString("PetCoverPhotoPath", Utils.petcoverphto);
        editor.putString("VetEmail", Utils.vetMail);
        editor.putString("VetMobilePhone", Utils.vetPhone);;
        editor.putInt("PetGender", Utils.petGender);
        editor.putInt("PetDrawablePath",R.drawable.bulldog);


        surname.setText(pref.getString("surname",""));
        username.setText(pref.getString("UserName",""));
        eposta.setText(pref.getString("Email",""));
        phone.setText(pref.getString("MobilePhone",""));
        edittext_city.setText(pref.getString("city",""));
        */
if(Utils.ownAccount) {
    fullname.setText(pref.getString("name", "") + " " + pref.getString("surname", ""));
    city.setText(pref.getString("city", ""));
    email.setText(pref.getString("Email", ""));
    phone.setText(pref.getString("MobilePhone", ""));
    veterinermail.setText(pref.getString("VetEmail", ""));
    veterinernumara.setText(pref.getString("VetMobilePhone", ""));
}
else   if(Utils.fromMyMatches) {
    new GetPetFullRequest(Utils.petID,LayoutTwo.this);
}
else if (!Utils.ownAccount){
    fullname.setText(Utils.name + " " + Utils.surname);
    city.setText(Utils.city);
    email.setText(Utils.eposta);
    phone.setText(Utils.phone);
    veterinermail.setText(Utils.vetMail);
    veterinernumara.setText(Utils.vetPhone);
}
        return root;
    }

    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {
        JSONObject object=(JSONObject)obj;
        try {
            if(object.getString("response")=="1")
            {
                fullname.setText(Utils.name + " " + Utils.surname);
                city.setText(Utils.city);
                email.setText(Utils.eposta);
                phone.setText(Utils.phone);
                veterinermail.setText(Utils.vetMail);
                veterinernumara.setText(Utils.vetPhone);
            }
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        // TODO Auto-generated method stub

    }


}
