package com.example.barut.petmatch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barut.petmatch.request.AddNewPetRequest;
import com.example.barut.petmatch.request.DeletePetRequest;
import com.example.barut.petmatch.request.UpdatePetRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PetProfileSettings extends BaseFragmentActivity implements AsyncListener{
    BrandTextView next;
    ImageView backButton;
    private EditText editTextName;
    private EditText editTextAge;
    private EditText editTextPhone;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextPasswordAgain;
    private EditText editTextCinsAdı;
    private BrandTextView textviewMale;
    private BrandTextView textvieFemale;
    private Integer gender;
    private Button autoCompleteButton;
    private CheckBox kısır;

    SharedPreferences pref;
    public static final String MyPREFERENCES = "MyPrefs" ;
    String filename = "profile.png";
    String filename2 = "cover.png";
    private static int	SELECT_PHOTO_2= 1000;
    private static int	SELECT_PHOTO= 1001;
    public static final int REQUEST_CODE_GALLERY      = 0x9;
    public static final int REQUEST_CODE_GALLERY_COVER = 0x12;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x10;
    public static final int REQUEST_CODE_CROP_IMAGE   = 0x11;
    public static final int REQUEST_CODE_CROP_IMAGE_COVER=0x8;
    private File mFileTemp;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final String TAG = "AddAPetActivity";
    private CircularImageView profileImage;
    private ImageView coverPhooto;
    Switch pendingAdoption;
    Switch pendingMatch;
    EditText pref1;
    EditText pref2;
    EditText pref3;
    EditText city;
    Button delete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_preferences);
        header_logo_layout.setVisibility(View.VISIBLE);
        headerMenu.setVisibility(View.VISIBLE);
        headerLayout.setClickable(true);
        setSlidingMenu();
       // LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View v = (View)inflater.inflate(R.layout.activity_pet_preferences, null);
        pref=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        next=(BrandTextView)findViewById(R.id.text_next);
        delete=(Button)findViewById(R.id.delete);
        backButton=(ImageView)findViewById(R.id.back_button_sign_up);
        editTextName=(EditText)findViewById(R.id.edittext_name);
        editTextAge=(EditText)findViewById(R.id.edittext_age);
        editTextPhone=(EditText)findViewById(R.id.edittext_phone);
        editTextEmail=(EditText)findViewById(R.id.veteriner_email);
        editTextCinsAdı=(EditText)findViewById(R.id.cins_adı);
        textviewMale=(BrandTextView)findViewById(R.id.erkek);
        textvieFemale=(BrandTextView)findViewById(R.id.kadin);
        coverPhooto=(ImageView)findViewById(R.id.cover);
        kısır=(CheckBox)findViewById(R.id.checkBox);
        pendingAdoption=(Switch)findViewById(R.id.pendingAdoption);
        pendingMatch=(Switch)findViewById(R.id.pendingMatch);
        pref1=(EditText)findViewById(R.id.pref1);
        pref2=(EditText)findViewById(R.id.pref2);
        pref3=(EditText)findViewById(R.id.pref3);
        city=(EditText)findViewById(R.id.city_edittext);
        profileImage=(CircularImageView)findViewById(R.id.profile_icon);
        editTextName.setText(pref.getString("PetName",""));
        editTextAge.setText(Integer.toString(pref.getInt("PetAge",1)));
        String vetmail=pref.getString("VetEmail","");
        String vetphone=pref.getString("VetMobilePhone","");
        editTextEmail.setText(pref.getString("VetEmail",""));
        editTextCinsAdı.setText(pref.getString("cinsadı",""));
        editTextPhone.setText(pref.getString("VetMobilePhone",""));
        city.setText(pref.getString("PetCity",""));

        if (pref.getInt("PetGender",2)==0) {
            textvieFemale.setTextColor(Color.rgb(250, 128, 104));
            textviewMale.setTextColor(Color.WHITE);
            gender = 0;
        } else if (pref.getInt("PetGender",2)==1) {
            textvieFemale.setTextColor(Color.WHITE);
            textviewMale.setTextColor(Color.rgb(250, 128, 104));
            gender = 1;
        }

        if (pref.getBoolean("isSterilized",false))
            kısır.setChecked(true);
        else
            kısır.setChecked(false);
if(Utils.pendingMatch) {
    pref1.setText(Utils.preff1);
    pref2.setText(Utils.preff2);
    pref3.setText(Utils.preff3);
}
        if(pref.getBoolean("pendingAdoption",false))
            pendingAdoption.setChecked(true);
        else
            pendingAdoption.setChecked(false);
        if(pref.getBoolean("pendingMatch",false))
            pendingMatch.setChecked(true);
        else
            pendingMatch.setChecked(false);

        coverPhooto.setImageResource(R.drawable.coverphoto);
        profileImage.setImageResource(R.drawable.bulldog);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.preff1=pref1.getText().toString();
                Utils.preff2=pref2.getText().toString();
                Utils.preff3=pref3.getText().toString();
                Utils.pendingAdoption=pendingAdoption.isChecked();
                Utils.pendingMatch=pendingMatch.isChecked();
                Utils.petname=editTextName.getText().toString();
                Utils.petType=editTextCinsAdı.getText().toString();
                Utils.petCity=city.getText().toString();
                Utils.petGender=gender;
                Utils.petAge=Integer.valueOf(editTextAge.getText().toString());
                Utils.isSterilized=kısır.isChecked();
                Utils.eposta=editTextEmail.getText().toString();
                Utils.phone=editTextPhone.getText().toString();

                new UpdatePetRequest(pref.getString("petID",""), pendingAdoption.isChecked(),  pendingMatch.isChecked(), Utils.preff1, Utils.preff2, Utils.preff3,
                        Utils.token, editTextName.getText().toString(), editTextCinsAdı.getText().toString(),
                        city.getText().toString(),  gender, Integer.valueOf(editTextAge.getText().toString()), kısır.isChecked(),
                        editTextEmail.getText().toString(),editTextPhone.getText().toString(), PetProfileSettings.this );
            }
        });
        pendingMatch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    pref1.setVisibility(View.VISIBLE);
                    pref2.setVisibility(View.VISIBLE);
                    pref3.setVisibility(View.VISIBLE);

                } else {
                    pref1.setVisibility(View.INVISIBLE);
                    pref2.setVisibility(View.INVISIBLE);
                    pref3.setVisibility(View.INVISIBLE);
                }
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog materialDialog = new MaterialDialog(PetProfileSettings.this);
                materialDialog.setMessage("Are you sure to delete the pet?")
                        .setPositiveButton("Delete", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();
                                new DeletePetRequest(pref.getString("petID",""),PetProfileSettings.this);
                            }
                        });
                materialDialog.setNegativeButton("Cancel", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();
                            }
                        }

                );
                materialDialog.show();

            }
        });
    }
    public void run(){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isSterilized",Utils.isSterilized);
        editor.putString("PetName", Utils.petname);
        editor.putString("cinsadı", Utils.petType);
        editor.putBoolean("pendingAdoption",Utils.pendingAdoption);
        editor.putBoolean("pendingMatch",Utils.pendingMatch);
        if(Utils.pendingMatch) {
            editor.putString("prefType1", Utils.preff1);
            editor.putString("prefType2", Utils.preff2);
            editor.putString("prefType3", Utils.preff3);
        }
        editor.putInt("PetAge",Utils.petAge);
        editor.putString("PetCity",Utils.petCity);
        editor.putString("PetProfilePhotoPath", Utils.petprofilephoto);
        editor.putString("PetCoverPhotoPath", Utils.petcoverphto);
        editor.putString("VetEmail", Utils.vetMail);
        editor.putString("VetMobilePhone", Utils.vetPhone);;
        editor.putInt("PetGender", Utils.petGender);
        editor.putInt("PetDrawablePath",R.drawable.bulldog);
        editor.commit();
    }
    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue, String extraData) {
        if(queue==RequestsQueue.DELETE_PET) {
            JSONObject jsonObject = (JSONObject) obj;
            try {
                String response = jsonObject.getString("response");
                String toast = "";
                if (response == "1") {
                    Intent intent = new Intent(PetProfileSettings.this, Dashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    toast = "Your pet " + editTextName.getText().toString() + " has been deleted.";
                    Utils.justDeletedPet=true;
                    intent.putExtra("toast", toast);

                    startActivity(intent);
                } else if (response == "0") {
                    toast = "Delete failed";
                    Toast t = new Toast(PetProfileSettings.this);
                    t.makeText(PetProfileSettings.this, toast, Toast.LENGTH_SHORT);
                } else if (response == "-2") {
                    toast = "Pet belongs to someone else";
                    Toast t = new Toast(PetProfileSettings.this);
                    t.makeText(PetProfileSettings.this, toast, Toast.LENGTH_SHORT);
                } else if (response == "-1") {
                    toast = "Pet does not exist";
                    Toast t = new Toast(PetProfileSettings.this);
                    t.makeText(PetProfileSettings.this, toast, Toast.LENGTH_SHORT);
                }
            } catch (JSONException j) {
                j.printStackTrace();
            }
        }
        if (queue==RequestsQueue.UPDATE_PET) {


            JSONObject jsonObject = (JSONObject) obj;
            try {
                String response = jsonObject.getString("response");
                String toast = "";
                if (response == "1") {
                    run();
                    toast="Pet Profile and Preferences Updated";
                    Intent i=new Intent(PetProfileSettings.this,UserActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("toast",toast);
                    startActivity(i);
                } else  {
                    toast = "Update failed";
                    Toast t = new Toast(PetProfileSettings.this);
                    t.makeText(PetProfileSettings.this, toast, Toast.LENGTH_SHORT);
                }
            } catch (JSONException j) {
                j.printStackTrace();
            }
        }
    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {

    }
}
