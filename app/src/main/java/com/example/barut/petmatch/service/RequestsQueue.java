package com.example.barut.petmatch.service;

public enum RequestsQueue {
	LOGIN(1),GET_DASHBOARD(2),CHECK_USER(3),LOG_OUT(4),REGISTER(5),UPLOAD_PROFILE_PICTURE(6),REGISTER_PHOTO(7),
	GET_USER_PETS(8),GET_PET_FULL(9),ADD_NEW_PET(10),DELETE_PET(11),UPDATE_PET(12),FIND_ADOPT(13),FIND_MATCH(14),
	GET_MYMATCHES(15),LIKE_ADOPT(16),LIKE_MATCH(17);


	private int value;
	private RequestsQueue(int value){
		this.value=value;
	}

	public int getValue(){
		return value;
	}
}
