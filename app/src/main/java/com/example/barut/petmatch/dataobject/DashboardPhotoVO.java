package com.example.barut.petmatch.dataobject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class DashboardPhotoVO implements Serializable {

	/**
	 "webformatURL": "https://pixabay.com/get/35bbf209db8dc9f2fa36746403097ae226b796b9e13e39d2_640.jpg",
	 "webformatWidth": 640,
	 "webformatHeight": 360,
	 */

	public String path = "";
	public String Id="";
	public String width="";
	public String height="";


	public DashboardPhotoVO(String imagepath,String w,String h) {

		path=imagepath;
		width=w;
		height=h;
	}
	
	public DashboardPhotoVO(JSONObject dict){
		
		
		try {
			if(dict.getString("id")!=null)
			Id=dict.getString("id");

			if(dict.getString("webformatURL")!=null)
				path = dict.getString("webformatURL");

			if(dict.isNull("webformatWidth")){
					width="720";
				}
			else{
					width=dict.getString("webformatWidth");
				}
			if(dict.isNull("webformatHeight")){
					height="720";
				}
			else{
					height=dict.getString("webformatHeight");
				}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
