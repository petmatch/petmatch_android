package com.example.barut.petmatch;

/**
 * Created by barut on 04.05.2017.
 */
import android.annotation.SuppressLint;
        import android.content.Context;
        import android.graphics.Color;
        import android.graphics.drawable.Drawable;
        import android.util.AttributeSet;
        import android.view.LayoutInflater;
        import android.widget.ImageView;
        import android.widget.RelativeLayout;
        import android.widget.TextView;



@SuppressLint("InflateParams") public class MenuItemObject extends RelativeLayout{
    public BrandTextView menuText;
    public String menuLabel;
    public ImageView icon;
    int d;

    public TextView getHeaderText() {
        return menuText;
    }

    public void setHeaderText(BrandTextView headerText) {
        this.menuText = headerText;
    }
    public void setres(int  d) {
        this.d = d;
    }

    public MenuItemObject(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public MenuItemObject(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }
    public static MenuItemObject getObject(Context context, String header,int d){

        LayoutInflater inflater = (LayoutInflater)context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        MenuItemObject mio;

        mio = (MenuItemObject)inflater.inflate(R.layout.menu_item_view, null);
        mio.menuLabel=header;
        mio.d=d;
        return mio;
    }
    private Boolean isSettled = false;
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        // TODO Auto-generated method stub
        super.onSizeChanged(w, h, oldw, oldh);
        setupLayout();

    }

    public void setupLayout(){
        if(!isSettled){
            isSettled = true;
            menuText = (BrandTextView)findViewById(R.id.menu_name);
            icon = (ImageView)findViewById(R.id.menu_icon);
            menuText.setText(menuLabel);
            menuText.setTextColor(0xff888888);
            icon.setImageResource(d);
        }
    }


}