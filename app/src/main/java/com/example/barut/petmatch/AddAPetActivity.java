package com.example.barut.petmatch;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.example.barut.petmatch.StorageStub.UserStub;
import com.example.barut.petmatch.dataobject.Pet;
import com.example.barut.petmatch.request.AddNewPetRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import eu.janmuller.android.simplecropimage.CropImage;

import static com.example.barut.petmatch.Dashboard.petArrayList;

public class AddAPetActivity extends BaseFragmentActivity implements AsyncListener {
    ActionBar actionBar;
    BrandTextView next;
    ImageView backButton;
    private EditText editTextName;
    private EditText editTextAge;
    private EditText editTextPhone;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextPasswordAgain;
    private EditText petCity;
    private Spinner editTextCinsAdı;
    private BrandTextView textviewMale;
    private BrandTextView textvieFemale;
    private Integer gender;
    private Button autoCompleteButton;
    private CheckBox kısır;

    SharedPreferences pref;
    public static final String MyPREFERENCES = "MyPrefs" ;
    String filename = "profile.png";
    String filename2 = "cover.png";
    private static int	SELECT_PHOTO_2= 1000;
    private static int	SELECT_PHOTO= 1001;
    public static final int REQUEST_CODE_GALLERY      = 0x9;
    public static final int REQUEST_CODE_GALLERY_COVER = 0x12;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x10;
    public static final int REQUEST_CODE_CROP_IMAGE   = 0x11;
    public static final int REQUEST_CODE_CROP_IMAGE_COVER=0x8;
    private File mFileTemp;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final String TAG = "AddAPetActivity";
    private CircularImageView profileImage;
    private ImageView coverPhooto;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        setContentView(R.layout.activity_add_apet);
        actionBar=getActionBar();
        actionBar.hide();
        next=(BrandTextView)findViewById(R.id.text_next);
        backButton=(ImageView)findViewById(R.id.back_button_sign_up);
        editTextName=(EditText)findViewById(R.id.edittext_name);
        editTextAge=(EditText)findViewById(R.id.edittext_age);
        editTextPhone=(EditText)findViewById(R.id.edittext_phone);
        editTextEmail=(EditText)findViewById(R.id.veteriner_email);
        editTextCinsAdı=(Spinner)findViewById(R.id.cins_adı);
        textviewMale=(BrandTextView)findViewById(R.id.erkek);
        textvieFemale=(BrandTextView)findViewById(R.id.kadin);
        kısır=(CheckBox)findViewById(R.id.checkBox);
        profileImage=(CircularImageView)findViewById(R.id.profile_icon);
        petCity=(EditText)findViewById(R.id.city_edittext);
        pref=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        List<String> categoriesList = new ArrayList<>();
        categoriesList.add("bulldog");
        categoriesList.add("pekinese");
        categoriesList.add("golden retriever");
        categoriesList.add("rottweiler");
        categoriesList.add("labrador");


        ArrayAdapter<String> adpSpinner
                = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,
                categoriesList);

        editTextCinsAdı.setAdapter(adpSpinner);
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        }
        else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
        coverPhooto = (ImageView)findViewById(R.id.cover);
        autoCompleteButton=(Button)findViewById(R.id.auto_complete);
        autoCompleteButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editTextName.setText("Fistik");
                editTextAge.setText("10");
                editTextEmail.setText("vethouse@gmail.com");
                editTextCinsAdı.setSelection(0);
                editTextPhone.setText("5392373959");
                petCity.setText("Istanbul");
                textvieFemale.setTextColor(Color.rgb(250, 128, 104));
                textviewMale.setTextColor(Color.WHITE);
                Utils.petGender=0;
                gender=0;
                kısır.setChecked(true);

            }
        });
        coverPhooto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery2();


            }
        });
        profileImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                openGallery();


            }
        });
        textvieFemale.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textvieFemale.setTextColor(Color.rgb(250, 128, 104));
                textviewMale.setTextColor(Color.WHITE);
                gender=0;
                Utils.petGender=0;
                return false;
            }
        });
        textviewMale.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textvieFemale.setTextColor(Color.WHITE);
                textviewMale.setTextColor(Color.rgb(250, 128, 104));
                gender=1;
                Utils.petGender=1;
                return false;
            }
        });
        backButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        next.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateFields().length()!=0){
                    final MaterialDialog materialDialog = new MaterialDialog(AddAPetActivity.this);
                    materialDialog.setMessage(validateFields())
                            .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    materialDialog.dismiss();
                                }
                            });
                    materialDialog.show();

                }

                else{
                    //Pet tobeadded= new Pet(R.drawable.bulldog,editTextName.getText().toString());
                    //petArrayList.add(tobeadded);
                    startServiceForUploadUserPhotoOrCover(Utils.petprofile,filename,false);
                    startServiceForUploadUserPhotoOrCover(Utils.petcover,filename2,true);
                    Utils.petname=editTextName.getText().toString();
                    Utils.petType=editTextCinsAdı.getSelectedItem().toString();
                    Utils.petCity=petCity.getText().toString();
                    Utils.petGender=gender;
                    Utils.petAge=Integer.valueOf(editTextAge.getText().toString());
                    Utils.isSterilized=kısır.isChecked();
                    Utils.eposta=editTextEmail.getText().toString();
                    Utils.phone=editTextPhone.getText().toString();
                    new AddNewPetRequest( Utils.token, editTextName.getText().toString(), editTextCinsAdı.getSelectedItem().toString(),
                            petCity.getText().toString(),  gender, Integer.valueOf(editTextAge.getText().toString()), kısır.isChecked(),
                            editTextEmail.getText().toString(),editTextPhone.getText().toString(), AddAPetActivity.this );
                }


                //	new PostUserRequest("1", "1", editTextName.getText().toString(),editTextSurName.getText().toString(), editTextPassword.getText().toString(), "Male", "16", editTextEmail.getText().toString(), editTextPhone.getText().toString(), editTextName.getText().toString()+" "+editTextSurName.getText().toString(), SignUpPage.this);
            }
        });
    }
    private void startServiceForUploadUserPhotoOrCover(Bitmap bitmap,String fileName,boolean isCover) {
        String path;FileOutputStream out=null;
        path = Environment.getExternalStorageDirectory().toString();
        File imageFile = new File(path,fileName);
        try {
            out = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        if(profileImage.getDrawable()!=null&&!isCover){
            System.out.println("startServiceForUploadPhoto: Profile");
            Utils.petprofilephoto=imageFile.toString();
            // new UploadUserPhotoOrCoverRequest(Utils.userId, isCover, imageFile, SignUpPagePhotoUpload.this);
        }
        if(coverPhooto.getDrawable()!=null&&isCover){
            Utils.petcoverphto=imageFile.toString();
            System.out.println("startServiceForUploadPhoto: Cover");
            //   new UploadUserPhotoOrCoverRequest(Utils.userId, isCover, imageFile, SignUpPagePhotoUpload.this);
        }
        //run();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap;


        if(requestCode==REQUEST_CODE_CROP_IMAGE){
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }

            bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
            profileImage.setImageBitmap(bitmap);
            //photo=bitmap;
            Utils.petprofile=bitmap;
            //isCover=false;
        }
        if(requestCode==REQUEST_CODE_CROP_IMAGE_COVER){
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }

            bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
            coverPhooto.setImageBitmap(bitmap);
            //coverPhoto=bitmap;
            Utils.petcover=bitmap;
            //isCover=true;
            coverPhooto.setBackgroundResource(R.drawable.image_shape);
        }
        if(requestCode==REQUEST_CODE_GALLERY_COVER){
            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage2();

            } catch (Exception e) {

                Log.e(TAG, "Error while creating temp file", e);
            }
        }
        if(requestCode==REQUEST_CODE_GALLERY){
            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage();

            } catch (Exception e) {

                Log.e(TAG, "Error while creating temp file", e);
            }
        }

        if(requestCode==SELECT_PHOTO){
            if(resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImage);
                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    profileImage.setImageBitmap(yourSelectedImage);
                    Utils.petprofile=yourSelectedImage;
                    //isCover=false;

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }

        if(requestCode==SELECT_PHOTO_2){
            if(resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImage);
                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    coverPhooto.setImageBitmap(yourSelectedImage);
                    Utils.petcover=yourSelectedImage;
                    coverPhooto.setBackgroundResource(R.drawable.rectangle);
                    //isCover=true;

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }

    }
    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }
    private void openGallery2() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY_COVER);
    }
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
    private void startCropImage() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }
    private void startCropImage2() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE_COVER);
    }
    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {
        if(queue==RequestsQueue.ADD_NEW_PET) {
            JSONObject jsonObject=(JSONObject)obj;
            try {
                if (jsonObject.getString("response") == "1") {
                    Utils.petID=jsonObject.getString("petID");
                    run();
                    System.out.println("Will move to the dashboardnew activity now. Pet added");
                    String toast="Pet "+editTextName.getText().toString()+" has been added\nClick on the overflow button to switch between your pets.";
                    Intent intent = new Intent(AddAPetActivity.this, Dashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("toast",toast);
                    startActivity(intent);
                }
            } catch (JSONException j) {
                j.printStackTrace();
            }
        }

    }


    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        // TODO Auto-generated method stub

    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    public void run(){

        SharedPreferences.Editor editor = pref.edit();
        editor.putString("petID",Utils.petID);
        editor.putString("PetName", editTextName.getText().toString());
        editor.putString("cinsadı", editTextCinsAdı.getSelectedItem().toString());
        editor.putBoolean("isSterilized",kısır.isChecked());
        editor.putInt("PetAge",Utils.petAge);
        editor.putString("PetCity",petCity.getText().toString());

        editor.putString("PetProfilePhotoPath", Utils.petprofilephoto);
        editor.putString("PetCoverPhotoPath", Utils.petcoverphto);

        editor.putString("VetEmail", editTextEmail.getText().toString());
        editor.putString("VetMobilePhone", editTextPhone.getText().toString());;
        editor.putInt("PetGender", gender);
        editor.putInt("PetDrawablePath",R.drawable.bulldog);
        editor.commit();
    }


    public String validateFields(){
        String text = "";




        if(editTextName.getText().toString().length()<1){
            text=text+"Lütfen isminizi yazınız.\n";
        }
        if(editTextAge.getText().toString().length()<1){
            text=text+"Lütfen köpeğin yaşını yazınız.\n";
        }
        if(editTextCinsAdı.getSelectedItem().toString().length()<1){
            text=text+"Lütfen hayvanın cinsini  yazınız.\n";
        }
        if(!isValidEmail(editTextEmail.getText().toString())){
            text=text+"Lütfen geçerli e-posta adresi giriniz.\n";
        }
        if(editTextPhone.getText().length()>0){
            if(editTextPhone.getText().length()!=10){
                text=text+"Telefon numarası 10 haneli olmalıdır.\n";

            }
        }


        return text;
    }

}
