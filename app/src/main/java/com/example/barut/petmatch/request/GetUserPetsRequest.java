package com.example.barut.petmatch.request;

import android.os.AsyncTask;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lENOVO on 31.05.2017.
 */

public class GetUserPetsRequest extends AsyncTask<Void, Void, String> {
    private String token="";
    AsyncListener asc;

    public GetUserPetsRequest(String token,AsyncListener asc) {
        System.out.println("LOG3: GET_USER_PETS");
        this.token = token;
        this.asc=asc;
        execute();
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
        try {
            System.out.println("LOG4: GET_USER_PETS");
            jsonObject.put("token",token);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        System.out.println("LOG5: GET_USER_PETS");
        return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_USERS + "getUserPets", jsonObject);

    }

    @Override
    protected void onPostExecute(String result) {
        System.out.println("LOG6: GET_USER_PETS");
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            System.out.println("LOG7: GET_USER_PETS");
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.GET_USER_PETS, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.GET_USER_PETS);
        }
        super.onPostExecute(result);
    }
}
