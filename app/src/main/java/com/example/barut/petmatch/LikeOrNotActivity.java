package com.example.barut.petmatch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barut.petmatch.dataobject.Pet;
import com.example.barut.petmatch.request.FindAdoptRequest;
import com.example.barut.petmatch.request.FindMatchRequest;
import com.example.barut.petmatch.request.LikeAdoptRequest;
import com.example.barut.petmatch.request.LikeMatchRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import loadimage.ImageLoader;

public class LikeOrNotActivity extends BaseFragmentActivity implements AsyncListener{
    TextView petname;
    TextView age;
    TextView cins;
    TextView cityy;
    BrandTextView pendingAdoption;
    BrandTextView pendingMatch;
    TextView cinsiyet;
    TextView kısırlık;
    ImageButton kalp;
    ImageButton cross;
    ImageButton info;
    ImageView petImage;
    ImageLoader img;
    private static Pet pet;
    private static String flag;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref=this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        setContentView(R.layout.activity_like_or_not);
        petname=(TextView)findViewById(R.id.petname);
        cityy=(TextView)findViewById(R.id.cityy);
        age=(TextView)findViewById(R.id.age);
        cins=(TextView)findViewById(R.id.cins);
        cinsiyet=(TextView)findViewById(R.id.cinsiyet);
        kısırlık=(TextView)findViewById(R.id.kısırlık);
        pendingAdoption=(BrandTextView)findViewById(R.id.pendingAdoption);
        pendingMatch=(BrandTextView)findViewById(R.id.pendingMatch);
        petImage=(ImageView)findViewById(R.id.imageView);
        kalp=(ImageButton)findViewById(R.id.kalp);
        info=(ImageButton)findViewById(R.id.info);
        cross=(ImageButton)findViewById(R.id.cross);
        header_logo_layout.setVisibility(View.VISIBLE);
        headerMenu.setVisibility(View.VISIBLE);
        headerLayout.setClickable(true);
        setSlidingMenu();
        flag=getIntent().getStringExtra("flag");
        if( flag.equals("adopt"))
            new FindAdoptRequest(LikeOrNotActivity.this);
        if( flag.equals("match"))
            new FindMatchRequest(LikeOrNotActivity.this, pref.getString("petID",""));


        kalp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if( flag.equals("adopt")) {
                    new LikeAdoptRequest(LikeOrNotActivity.this,Utils.petID);

                }
                if( flag.equals("match")) {
                    new LikeMatchRequest(LikeOrNotActivity.this,pref.getString("petID",""),Utils.petID);

                }
//listeden sil ama mymatches'e ekle
            }
        });
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//listeden silmek için request
                if( flag.equals("adopt"))
                    new FindAdoptRequest(LikeOrNotActivity.this);
                if( flag.equals("match"))
                    new FindMatchRequest(LikeOrNotActivity.this, Utils.petID);
            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LikeOrNotActivity.this,UserActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Utils.ownAccount=false;
                Utils.fromMyMatches=false;
                i.putExtra("petname" ,petname.getText());
                i.putExtra("city", cityy.getText());
                i.putExtra("age", age.getText());
                i.putExtra("cins", cins.getText());
                i.putExtra("cinsiyet", cinsiyet.getText());
                i.putExtra("kısırlık", kısırlık.getText());
                startActivity(i);
            }
        });
        petImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LikeOrNotActivity.this,UserActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Utils.ownAccount=false;
                Utils.fromMyMatches=false;
                i.putExtra("petname" ,petname.getText());
                i.putExtra("city", cityy.getText());
                i.putExtra("age", age.getText());
                i.putExtra("cins", cins.getText());
                i.putExtra("cinsiyet", cinsiyet.getText());
                i.putExtra("kısırlık", kısırlık.getText());
                startActivity(i);
            }
        });
    }

    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue, String extraData) {
        if (queue==RequestsQueue.FIND_ADOPT) {
            JSONObject jsonObject=(JSONObject) obj;
            pet=new Pet(jsonObject,true);
            petname.setText(Utils.petname);
            cityy.setText(Utils.petCity);
            age.setText(Integer.toString(Utils.petAge));
            cins.setText(Utils.petType);
            if (Utils.petGender==1)
                cinsiyet.setText("Erkek");
            else
                cinsiyet.setText("Kız");
            if(Utils.isSterilized)
                kısırlık.setText("Kısır");
            else
                kısırlık.setText("Kısır Değil");
            pendingMatch.setText("pendingMatch");
            pendingAdoption.setText("pendingAdoption");
            if(Utils.pendingMatch) {
                pendingMatch.setTextColor(Color.rgb(250, 128, 104));

            }
            else {
                pendingMatch.setTextColor((Color.BLACK));
            }

            if(Utils.pendingAdoption) {
                pendingAdoption.setTextColor(Color.rgb(250, 128, 104));

            }
            else {
                pendingAdoption.setTextColor((Color.BLACK));
            }

            img=new ImageLoader(this);
            img.DisplayImage(Utils.petprofilephoto,petImage);


        }
        if (queue==RequestsQueue.LIKE_MATCH) {
            JSONObject jsonObject=(JSONObject) obj;
            try {
                if (jsonObject.getString("response") == "1") {
                    new FindMatchRequest(LikeOrNotActivity.this, Utils.petID);
                }
            } catch(JSONException j) {
                j.printStackTrace();
            }
        }
        if (queue==RequestsQueue.LIKE_ADOPT) {
            JSONObject jsonObject=(JSONObject) obj;
            try {
                if (jsonObject.getString("response") == "1") {
                    new FindAdoptRequest(LikeOrNotActivity.this);
                }
            } catch(JSONException j) {
                j.printStackTrace();
            }
        }
        if (queue==RequestsQueue.FIND_MATCH) {
            JSONObject jsonObject=(JSONObject) obj;
            pet=new Pet(jsonObject,true);
            petname.setText(Utils.petname);
            cityy.setText(Utils.petCity);
            age.setText(Integer.toString(Utils.petAge));
            cins.setText(Utils.petType);
            if (Utils.petGender==1)
                cinsiyet.setText("Erkek");
            else
                cinsiyet.setText("Kız");
            if(Utils.isSterilized)
                kısırlık.setText("Kısır");
            else
                kısırlık.setText("Kısır Değil");
            pendingMatch.setText("pendingMatch");
            pendingAdoption.setText("pendingAdoption");
            if(Utils.pendingMatch) {
                pendingMatch.setTextColor(Color.rgb(250, 128, 104));

            }
            else {
                pendingMatch.setTextColor((Color.BLACK));
            }

            if(Utils.pendingAdoption) {
                pendingAdoption.setTextColor(Color.rgb(250, 128, 104));

            }
            else {
                pendingAdoption.setTextColor((Color.BLACK));
            }

            img=new ImageLoader(this);
            img.DisplayImage(Utils.petprofilephoto,petImage);

        }
    }


    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {

    }
}
