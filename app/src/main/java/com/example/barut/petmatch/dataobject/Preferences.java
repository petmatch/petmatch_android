package com.example.barut.petmatch.dataobject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by barut on 21.03.2017.
 */
public class Preferences {
   int minPetAge;				// integer
    int maxPetAge;				// integer
    String gender;				// string
    String city;				// string
    List<String> petType=new ArrayList<String>(); // ArrayList<String>

public Preferences(){}

    public Preferences(int minPetAge, int maxPetAge, String gender, String city, List<String> petType) {
        this.minPetAge = minPetAge;
        this.maxPetAge = maxPetAge;
        this.gender = gender;
        this.city = city;
        this.petType = petType;
    }
}
