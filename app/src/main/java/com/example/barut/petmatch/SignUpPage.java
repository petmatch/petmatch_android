package com.example.barut.petmatch;


import android.os.Bundle;
import android.view.View;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.example.barut.petmatch.request.CheckUserRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpPage extends BaseFragmentActivity implements AsyncListener {
    ActionBar actionBar;
    BrandTextView next;
    ImageView backButton;
    private EditText city_edittext;
    private EditText editTextName;
    private EditText editTextSurName;
    private EditText editTextPhone;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextPasswordAgain;
    private EditText editTextUserName;
    private BrandTextView textviewMale;
    private BrandTextView textvieFemale;
    private Integer gender;
    private Button autoCompleteButton;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        setContentView(R.layout.activity_sign_up_page);
        actionBar=getActionBar();
        actionBar.hide();
        next=(BrandTextView)findViewById(R.id.text_next);
        backButton=(ImageView)findViewById(R.id.back_button_sign_up);
        editTextName=(EditText)findViewById(R.id.edittext_name);
        editTextSurName=(EditText)findViewById(R.id.edittext_surname);
        editTextPhone=(EditText)findViewById(R.id.edittext_phone);
        editTextEmail=(EditText)findViewById(R.id.edittext_email);
        editTextUserName=(EditText)findViewById(R.id.edittext_username);
        textviewMale=(BrandTextView)findViewById(R.id.erkek);
        textvieFemale=(BrandTextView)findViewById(R.id.kadin);
        city_edittext=(EditText)findViewById(R.id.city_edittext);
        autoCompleteButton=(Button)findViewById(R.id.auto_complete);
        autoCompleteButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editTextName.setText("Berkin");
                editTextSurName.setText("Barut");
                editTextEmail.setText("barut@sabanciuniv.edu");
                editTextUserName.setText("berkin.deneme2");
                editTextPhone.setText("5392373959");
                city_edittext.setText("Istanbul");
                textvieFemale.setTextColor(Color.WHITE);
                textviewMale.setTextColor(Color.rgb(250, 128, 104));
                Utils.petGender=1;
                gender=1;

            }
        });
        textvieFemale.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textvieFemale.setTextColor(Color.rgb(250, 128, 104));
                textviewMale.setTextColor(Color.WHITE);
                gender=0;
                return false;
            }
        });
        textviewMale.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textvieFemale.setTextColor(Color.WHITE);
                textviewMale.setTextColor(Color.rgb(250, 128, 104));
                gender=1;
                return false;
            }
        });
        backButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        next.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if(validateFields().length()!=0){
                    final MaterialDialog materialDialog = new MaterialDialog(SignUpPage.this);
                    materialDialog.setMessage(validateFields())
                            .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    materialDialog.dismiss();
                                }
                            });
                    materialDialog.show();

                }

                else{
                    new CheckUserRequest(editTextEmail.getText().toString(),SignUpPage.this);

                }


                //	new PostUserRequest("1", "1", editTextName.getText().toString(),editTextSurName.getText().toString(), editTextPassword.getText().toString(), "Male", "16", editTextEmail.getText().toString(), editTextPhone.getText().toString(), editTextName.getText().toString()+" "+editTextSurName.getText().toString(), SignUpPage.this);
            }
        });
    }


    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {
        try {
            JSONObject jsonObject=(JSONObject) obj;
            if (jsonObject.getString("response")=="0") {
                Intent intent = new Intent(getApplicationContext(), SignUpPagePhotoUpload.class);
                intent.putExtra("name", editTextName.getText().toString());
                intent.putExtra("lastname", editTextSurName.getText().toString());
                intent.putExtra("phone", editTextPhone.getText().toString());
                intent.putExtra("email", editTextEmail.getText().toString());
                intent.putExtra("gender", gender);
                intent.putExtra("username", editTextUserName.getText().toString());
                intent.putExtra("city",city_edittext.getText().toString());
                startActivity(intent);
            }
            else if (jsonObject.getString("response")=="1")
            {
                final MaterialDialog materialDialog = new MaterialDialog(SignUpPage.this);
                materialDialog.setMessage("The user already exists!" +
                        "Please a try a new mail address.")
                        .setPositiveButton(android.R.string.yes, new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();
                            }
                        });
                materialDialog.show();
            }
            else {
                final MaterialDialog materialDialog = new MaterialDialog(SignUpPage.this);
                materialDialog.setMessage("A problem occured on the server.")
                        .setPositiveButton(android.R.string.yes, new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();
                            }
                        });
                materialDialog.show();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        // TODO Auto-generated method stub

    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }



    public String validateFields(){
        String text = "";




        if(editTextName.getText().toString().length()<1){
            text=text+"Lütfen isminizi yazınız.\n";
        }
        if(editTextSurName.getText().toString().length()<1){
            text=text+"Lütfen soy isminizi yazınız.\n";
        }
        if(editTextUserName.getText().toString().length()<1){
            text=text+"Lütfen kullanıcı adı  belirleyiniz.\n";
        }
        if(!isValidEmail(editTextEmail.getText().toString())){
            text=text+"Lütfen geçerli e-posta adresi giriniz.\n";
        }
        if(editTextPhone.getText().length()>0){
            if(editTextPhone.getText().length()!=10){
                text=text+"Telefon numaranız 10 haneli olmalıdır.\n";

            }
        }


        return text;
    }

}
