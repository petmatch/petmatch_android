package com.example.barut.petmatch.request;

import android.os.AsyncTask;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lENOVO on 02.06.2017.
 */

public class LikeMatchRequest extends AsyncTask<Void, Void, String> {
    String myPetID;
    String otherPetID;
    private AsyncListener asc;
    public LikeMatchRequest(AsyncListener asc,String myPetID,String otherPetID) {
        super();
        try {
            this.asc=asc;
            this.myPetID=myPetID;
            this.otherPetID=otherPetID;
            execute();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
        try {


            jsonObject.put("token", Utils.token);
            jsonObject.put("myPetID",myPetID);
            jsonObject.put("otherPetID",otherPetID);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_PETS + "likeMatch", jsonObject);
    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.LIKE_MATCH, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.LIKE_MATCH);
        }
        super.onPostExecute(result);
    }
}

