package com.example.barut.petmatch.request;

import android.os.AsyncTask;
import android.util.Log;


import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DashboardPhotosRequest extends AsyncTask<Void, Void, String> {
	
	
	private AsyncListener asc;
	private String currentpage;
	private String itemCount;
	public DashboardPhotosRequest(AsyncListener asc, String currentpage, String itemCount) {
		super();
		this.asc = asc;
		this.currentpage = currentpage;
		this.itemCount = itemCount;
		execute();
	}

	@Override
	protected String doInBackground(Void... params) {

		/*JSONObject jsonObject=new JSONObject();
		try {


			jsonObject.put("currentPage", currentpage);
			jsonObject.put("itemCount", itemCount);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */

		return ServiceManager.sendGET(ApplicationConstants.SERVER_ADRESS_DOG_PHOTOS+"per_page="+itemCount+"&page="+currentpage);
	}

	@Override
	protected void onPostExecute(String result) {

		try {
			JSONObject obj=new JSONObject(result);
			JSONArray jsonArr=obj.getJSONArray("hits");

			asc.asyncOperationSucceded(jsonArr, RequestsQueue.GET_DASHBOARD, "");
		} catch (JSONException e) {
			e.printStackTrace();
			asc.asyncOperationFailed(null, RequestsQueue.GET_DASHBOARD);
		}
		
		
		
		super.onPostExecute(result);
	}
	
	

}
