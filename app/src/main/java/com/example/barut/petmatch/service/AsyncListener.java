package com.example.barut.petmatch.service;



public interface AsyncListener {

	public void asyncOperationSucceded(Object obj, RequestsQueue queue, String extraData);
	public void asyncOperationFailed(Object obj, RequestsQueue queue);
	
}
