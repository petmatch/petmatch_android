package com.example.barut.petmatch.dataobject;

import com.example.barut.petmatch.R;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by barut on 21.03.2017.
 */
public class Pet {
    //  List<Disease> diseaseList; // disease list
    //List<Vaccine> vaccineList;
    //List<Photo> photoList;							// picture object
    // Vet vet;
    //String ownerID;			// string
    //String petSpecie; 				// string


    public String petName; 				// string
    public String id="";
    public String petProfilePhotoPath ="";
    public static int petProfileDrawableID= R.drawable.bulldog;

   /* String petType; 				// string
    int petGender; 				// string
    int petAge; 					// integer
    boolean isSterilized= false; 	// boolean
    String bio="";			// string
    String vetMail="";
    String vetPhone="";
    String petCity=""; */

   /* "pendingAdoption": true,				// boolean
            "pendingMatch": true,				// boolean
            "prefType": ["golden", "bulldog", "rottweiler"]	// ArrayList<String> */

    public Pet(int petProfileDrawableID,String petname) {
        this.petName=petname;
        this.petProfileDrawableID=petProfileDrawableID;
    }
    public  Pet(){}
    public Pet(String name, String path, String id)
    {
        this.petName=name;
        this.petProfilePhotoPath =path;
        this.id=id;
    }
    public Pet(JSONObject obj) {
        try {
            Utils.petID=obj.getString("petID"); //
            Utils.petCity=obj.getString("petCity");
            Utils.vetMail=obj.getString("vetMail");
            Utils.isSterilized=obj.getBoolean("isSterilized");
            Utils.vetPhone=obj.getString("vetPhone");
            Utils.petprofilephoto=obj.getString("petProfilePhotoPath");
            Utils.petname=obj.getString("petName");
            Utils.petAge=obj.getInt("petAge");
            Utils.petType=obj.getString("petType");
            Utils.petGender=obj.getInt("petGender");
            Utils.pendingAdoption=obj.getBoolean("pendingAdoption");
            Utils.pendingMatch=obj.getBoolean("pendingMatch");
            Utils.name=obj.getString("ownerName");
            Utils.surname=obj.getString("ownerSurname");
            Utils.phone=obj.getString("phone");
            Utils.eposta=obj.getString("eMail");
            Utils.city=obj.getString("city");
            if(Utils.pendingMatch) {
                Utils.preff1 = obj.getString("prefType1");
                Utils.preff2 = obj.getString("prefType2");
                Utils.preff3 = obj.getString("prefType3");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public Pet(JSONObject obj,boolean ownerIncluded) {
        try {
            Utils.petID=obj.getString("petID"); //
            Utils.petCity=obj.getString("petCity");
            Utils.vetMail=obj.getString("vetMail");
            Utils.isSterilized=obj.getBoolean("isSterilized");
            Utils.vetPhone=obj.getString("vetPhone");
            Utils.petprofilephoto=obj.getString("petProfilePhotoPath");
            Utils.petname=obj.getString("petName");
            Utils.petAge=obj.getInt("petAge");
            Utils.petType=obj.getString("petType");
            Utils.petGender=obj.getInt("petGender");

            Utils.name=obj.getString("ownerName");
            Utils.surname=obj.getString("ownerSurname");
            Utils.phone=obj.getString("phone");
            Utils.eposta=obj.getString("eMail");
            Utils.city=obj.getString("city");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public Pet(String name, String path) {
        this.petName=name;
        this.petProfilePhotoPath =path;
    }

}
