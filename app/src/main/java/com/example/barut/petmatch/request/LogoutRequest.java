package com.example.barut.petmatch.request;

import android.os.AsyncTask;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by barut on 22.03.2017.
 */
public class LogoutRequest extends AsyncTask<Void, Void, String> {
    private String sessionKey;
    private AsyncListener asc;
    public LogoutRequest(String sessionKey,AsyncListener asc) {
        super();
        try {
            this.asc = asc;
            this.sessionKey = sessionKey;
            execute();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
        try {


            jsonObject.put("token", sessionKey);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_USERS + "logout", jsonObject);
    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.LOG_OUT, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.LOG_OUT);
        }
        super.onPostExecute(result);
    }
}
