package com.example.barut.petmatch;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;

public class PrivacyPolicyActivity extends BaseFragmentActivity implements AsyncListener {


    ImageView imageViewX;
    ImageView imageViewCheck;
    BrandTextView textViewKul;
    BrandTextView textViewGuv;
    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);

        setContentView(R.layout.activity_privacy_policy);
        imageViewX=(ImageView)findViewById(R.id.remove_button);
        imageViewCheck=(ImageView)findViewById(R.id.check_icon);
        textViewKul=(BrandTextView)findViewById(R.id.text_kullanim);
        textViewGuv=(BrandTextView)findViewById(R.id.text_guvenlik);

        imageViewX.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();

            }
        });

        imageViewCheck.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                imageViewCheck.setBackgroundResource(R.drawable.check_aktif);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","ok");
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        });

        textViewKul.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                final MaterialDialog materialDialog = new MaterialDialog(PrivacyPolicyActivity.this);
                materialDialog.setMessage("Privacy Policy").setTitle("Kullanım KoşullarıÖ")
                        .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();

                            }
                        });
                materialDialog.show();

            }
        });
        textViewGuv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                final MaterialDialog materialDialog = new MaterialDialog(PrivacyPolicyActivity.this);
                materialDialog.setMessage("Privacy Policy").setTitle("Gizlilik Politikası")
                        .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();

                            }
                        });
                materialDialog.show();

            }
        });

        actionBar=getActionBar();
        actionBar.hide();
    }

    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {
        // TODO Auto-generated method stub

    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        // TODO Auto-generated method stub

    }



}
