package com.example.barut.petmatch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.example.barut.petmatch.StorageStub.UserStub;
import com.example.barut.petmatch.service.AsyncListener;

import org.json.JSONObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.ApplicationConstants;
import com.example.barut.petmatch.utils.Utils;

import eu.janmuller.android.simplecropimage.CropImage;
import loadimage.ImageLoader;

import static java.lang.Thread.sleep;


public class AccountSettings extends BaseFragmentActivity {
    static boolean isChangedProfile=false;
    static boolean isChangedCover=false;
    Button buttonSifre;
    EditText name;
    EditText surname;
    EditText username;
    EditText eposta;
    EditText phone;
    EditText changepassword;
    EditText edittext_city;
    BrandTextView erkek;
    BrandTextView kadin;
    CircularImageView profile;
    ImageView cover;
    ImageLoader imageLoader;
    String filename = "profile.png";
    String filename2 = "cover.png";
    static int i=0;
    static int updatedPreferences=0;
    SharedPreferences pref;
    public static final String MyPREFERENCES = "MyPrefs" ;
    private static final int	COVER_FROM_GALLERY=2;
    private static final int	COVER_FROM_GALLERY2=21;
    boolean isccover;
    private static int	SELECT_PHOTO_2= 1000;
    private static int	SELECT_PHOTO= 1001;
    public static final int REQUEST_CODE_GALLERY      = 0x9;
    public static final int REQUEST_CODE_GALLERY_COVER = 0x12;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x10;
    public static final int REQUEST_CODE_CROP_IMAGE   = 0x11;
    public static final int REQUEST_CODE_CROP_IMAGE_COVER=0x8;
    private File      mFileTemp;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final String TEMP_PHOTO_FILE_NAME_2 = "temp2_photo.jpg";
    Bitmap photo;
    Bitmap coverPhoto;
    private String userIconPath;
    private String userCoverPath;
    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        pref=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //userIconPath=getIntent().getStringExtra("userIconPath");
        //userCoverPath=getIntent().getStringExtra("userCoverPath");
        //Utils.userId=getIntent().getStringExtra("Id");
        setContentView(R.layout.activity_account_settings);
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        }
        else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
        name=(EditText)findViewById(R.id.edittext_name);
        surname=(EditText)findViewById(R.id.edittext_surname);
        buttonSifre=(Button)findViewById(R.id.sifre_button);
        username=(EditText)findViewById(R.id.edittext_username);
        eposta=(EditText)findViewById(R.id.edittext_email);
        phone=(EditText)findViewById(R.id.edittext_phone);
        erkek=(BrandTextView)findViewById(R.id.erkek);
        kadin=(BrandTextView)findViewById(R.id.kadin);
        changepassword=(EditText)findViewById(R.id.edittext_sifredegistir);
        edittext_city=(EditText)findViewById(R.id.edittext_city);
        profile=(CircularImageView)findViewById(R.id.upload_profile_icon);
        cover=(ImageView)findViewById(R.id.cover);
       // final UserStub user=new UserStub();
        //run();
        name.setText(pref.getString("name",""));
        surname.setText(pref.getString("surname",""));
        username.setText(pref.getString("UserName",""));
        eposta.setText(pref.getString("Email",""));
        phone.setText(pref.getString("MobilePhone",""));
        edittext_city.setText(pref.getString("city",""));
        imageLoader=new ImageLoader(this);
        if(isChangedProfile) {
            profile.setImageBitmap(Utils.profile);
        } else {
            profile.setImageResource(pref.getInt("DrawableProfilePath",0));
        }

        if(isChangedCover)
        {
            cover.setImageBitmap(Utils.cover);
        }
        else {
            cover.setImageResource(pref.getInt("DrawableCoverPath", 0));
        }
       // new GetUserRequest(AccountSettings.this, Utils.userId);
        //if(pref.getString("ProfilePhotoPath","")!=null)
        //		imageLoader.DisplayImage(ApplicationConstants.SERVER_ADRESS_PROFILE_PIC+pref.getString("ProfilePhotoPath", ""), profile);
		/*else
		imageLoader.DisplayImage(ApplicationConstants.SERVER_ADRESS_PROFILE_PIC+userIconPath, profile);*/
        //if(pref.getString("CoverPhotoPath","")!=null)
        //		imageLoader.DisplayImage(ApplicationConstants.SERVER_ADRESS_PROFILE_PIC+pref.getString("CoverPhotoPath", ""), cover);
		/*else
		imageLoader.DisplayImage(ApplicationConstants.SERVER_ADRESS_PROFILE_PIC+userCoverPath, cover);*/
        //imageLoader.DisplayImage(ApplicationConstants.SERVER_ADRESS_PROFILE_PIC+Utils.profilephoto, profile);
        //imageLoader.DisplayImage(ApplicationConstants.SERVER_ADRESS_PROFILE_PIC+Utils.coverphto, cover);
        changepassword.setKeyListener(null);
        edittext_city.setKeyListener(null);
        headerText.setText("Tamam");
        eposta.setKeyListener(null);
        username.setKeyListener(null);
        headerttnew.setText("Ayarlar");
        setSlidingMenu();
        buttonSifre.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
               // Intent intent=new Intent(AccountSettings.this, ChangePasswordActivity.class);
               // startActivity(intent);

            }
        });


        headerText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if(phone.getText().length()>0&&phone.getText().length()!=10){
                    //if(phone.getText().length()!=10){

                    final MaterialDialog materialDialog = new MaterialDialog(AccountSettings.this);
                    materialDialog.setMessage("Telefon numaranız 10 haneli olmalıdır.\n")
                            .setPositiveButton(android.R.string.yes, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    materialDialog.dismiss();
                                }
                            });
                    materialDialog.show();


                }
                else{
                   // new UpdateUserRequest(name.getText().toString()+" "+surname.getText().toString(),username.getText().toString(), name.getText().toString(), surname.getText().toString(), pref.getString("password", ""), eposta.getText().toString(), phone.getText().toString(), AccountSettings.this);
                   // startServiceForUploadUserPhotoOrCover(drawableToBitmap(profile.getDrawable()),filename,false);
                    Utils.name=name.getText().toString();
                    Utils.surname=surname.getText().toString();
                    Utils.username=username.getText().toString();

                Utils.eposta=eposta.getText().toString();
                    Utils.phone=phone.getText().toString();
                    Utils.city=edittext_city.getText().toString();
                    startServiceForUploadUserPhotoOrCover(Utils.profile, filename, false);
                    startServiceForUploadUserPhotoOrCover(Utils.cover, filename2, true);
                    run();
                    String toast="Account Settings Updated";
                    Intent i=new Intent(AccountSettings.this,Dashboard.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("toast",toast);
                    startActivity(i);
                }

            }
        });



        changepassword.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Intent intent=new Intent(AccountSettings.this, ChangePasswordActivity.class);
                //startActivity(intent);

            }
        });



        profile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                openGallery();

            }
        });
        cover.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                openGallery2();

            }
        });

    }

    private void startServiceForUploadUserPhotoOrCover(Bitmap bitmap,String fileName,boolean isCover) {
        String path;FileOutputStream out=null;
        path = Environment.getExternalStorageDirectory().toString();
        File imageFile = new File(path,fileName);
        try {
            out = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        System.out.println("Will choose a service to enter in startService function now.");
        if(profile.getDrawable()!=null&&!isCover){
            System.out.println("startServiceForUploadPhoto: Profile");
            //new UploadUserPhotoOrCoverRequest(Utils.userId, isCover, imageFile, AccountSettings.this);
            Log.i("PHOTO","Profile photo path: "+Utils.profilephoto);
        }
        if(cover.getDrawable()!=null&&isCover){
            System.out.println("startServiceForUploadPhoto: Cover");
           // new UploadUserPhotoOrCoverRequest(Utils.userId, isCover, imageFile, AccountSettings.this);
            Log.i("PHOTO","Profile photo path: "+Utils.coverphto);
        }
        //run();
    }


    public void run() {
        Editor editor = pref.edit();
        editor.putBoolean("isLogin", true);
        editor.putString("name", Utils.name);
        editor.putString("surname", Utils.surname);
        editor.putString("UserName", Utils.username);
        editor.putString("ProfilePhotoPath", Utils.profilephoto);
        editor.putString("CoverPhotoPath", Utils.coverphto);
        editor.putString("Email", Utils.eposta);
        editor.putString("MobilePhone", Utils.phone);
        editor.putString("city",Utils.city);
        editor.putInt("DrawableProfilePath", Utils.drawableProfilePath);
        editor.putInt("DrawableCoverPath", Utils.drawableCoverPath);
        editor.commit();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap;


        if(requestCode==REQUEST_CODE_CROP_IMAGE){
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }

            bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());

            profile.setImageBitmap(bitmap);
            photo=bitmap;



        }
        if(requestCode==REQUEST_CODE_CROP_IMAGE_COVER){
            String path = data.getStringExtra(CropImage.IMAGE_PATH);
            if (path == null) {

                return;
            }

            bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
            cover.setImageBitmap(bitmap);
            coverPhoto=bitmap;
        }
        if(requestCode==REQUEST_CODE_GALLERY_COVER){
            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage2();

            } catch (Exception e) {


            }
        }
        if(requestCode==REQUEST_CODE_GALLERY){
            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                copyStream(inputStream, fileOutputStream);
                fileOutputStream.close();
                inputStream.close();

                startCropImage();

            } catch (Exception e) {


            }
        }

        if(requestCode==SELECT_PHOTO){
            if(resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImage);
                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    profile.setImageBitmap(yourSelectedImage);
                    Utils.profile=yourSelectedImage;
                    isChangedProfile=true;
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }

        if(requestCode==SELECT_PHOTO_2){
            if(resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImage);
                    Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                    cover.setImageBitmap(yourSelectedImage);
                    Utils.cover=yourSelectedImage;
                    isChangedCover=true;
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }
    private void openGallery2() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY_COVER);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
    private void startCropImage() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }
    private void startCropImage2() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE_COVER);
    }
    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

}
