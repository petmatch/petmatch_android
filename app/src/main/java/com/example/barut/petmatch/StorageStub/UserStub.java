package com.example.barut.petmatch.StorageStub;

import android.graphics.Bitmap;

import com.example.barut.petmatch.R;
import com.example.barut.petmatch.dataobject.Pet;
import com.example.barut.petmatch.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by barut on 10.05.2017.
 */

public  class UserStub implements Serializable {
    /**
     *
     */

    //petList: 						// pet object(s)
    //adoptedList:							// pet object(s)

	/*profilePicture:								// picture object
	{
		_id: “2348752”;							// string
		link: “petmatch.com/pics/altug_the_java_developer.jpg”;		// string
		width: 400;								// integer
		height: 500;								// integer
	}*/

	/*Preferences:				// preference object
	{
		minPetAge: 3;				// integer
		maxPetAge: 4;				// integer
		gender: “male”;				// string
		city: “İzmir”;				// string
		petType[3]:				// ArrayList<String>
		{
			petType[0]: “Golden”;				// string
			petType[1]: “Bulldog”;			// string
			petType[2]: ”Rottweiler”;			// string
		}
	}*/



    /////



    private String username="";
    private String surname="";
    private String fullname="";
    private String biography="";
    private String sessionKey="";
    private String name="";
    private String ownerID="";
    private String eMail="";
    private String phone="";
    private String city="";
    private String  birthday="";
    private String profilePhotoPath="";
    private String coverPhotoPath="";
    private List<Pet> petList = new ArrayList<Pet>();
    private Boolean gender;
    private boolean isPremium=false;
    private int drawableProfilePath;
    private int drawableCoverPath;

    public  UserStub(){



                ownerID= "1";
        Utils.userId=ownerID;
                name="Berkin";
            Utils.name=name;
        username="berkinbarut";
        Utils.username = username;

      surname="Barut";
        Utils.surname = surname;
fullname="Berkin Barut";
        biography = "Bu bir bio.";
Utils.biography=biography;

                sessionKey="111";
			/*if(dict.has("petList")) {
				JSONArray array = dict.getJSONArray("petList");
				for(int i = 0 ; i < array.length() ; i++){
					petList.add(array.getJSONObject(i).getObject("interestKey"));
				}
				petList = dict.get("petList");
			}*/
drawableProfilePath= R.drawable.profilephoto;
        Utils.drawableProfilePath=drawableProfilePath;
        drawableCoverPath=R.drawable.coverphoto;
        Utils.drawableCoverPath=drawableCoverPath;
                city="Istanbul";
            Utils.city=city;
                eMail="barut@sabanciuniv.edu";
            Utils.eposta=eMail;
                phone="05392373959";
           Utils.phone=phone;
                profilePhotoPath = "petmatch.com/pics/altug_the_java_developer.jpg";
        Utils.profilephoto=profilePhotoPath;
            coverPhotoPath="petmatch.com/pics/altug_the_java_developer.jpg";
        Utils.coverphto=coverPhotoPath;

                birthday="1994-08-21";

                isPremium=false;
        gender=true;



    }

    public void setDrawableProfilePath(int id) {this.drawableProfilePath=id;}
    public int getDrawableProfilePath() { return this.drawableProfilePath; }

    public int getDrawableCoverPath() {
        return drawableCoverPath;
    }

    public void setDrawableCoverPath(int drawableCoverPath) {
        this.drawableCoverPath = drawableCoverPath;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return city;
    }

    public void setLocation(String location) {
        this.city = location;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getProfilePhotoPath() {
        return profilePhotoPath;
    }

    public void setProfilePhotoPath(String profilePhotoPath) {
        this.profilePhotoPath = profilePhotoPath;
    }

    public String getCoverPhotoPath() {
        return coverPhotoPath;
    }

    public void setCoverPhotoPath(String coverPhotoPath) {
        this.coverPhotoPath = coverPhotoPath;
    }

    public List<Pet> getPetList() {
        return petList;
    }

    public void setPetList(List<Pet> petList) {
        this.petList = petList;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }
}
