package com.example.barut.petmatch;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class BaseFragmentActivity extends FragmentActivity {

	private LinearLayout contentHolder;
	ActionBar actionBar;
	public SlidingMenu slidingMenu;
	RelativeLayout headerLayout;
	public static BrandTextView headerText;
	RelativeLayout headerMenu;
	ImageView overFlowButton;
	ImageView notification;
	protected RelativeLayout backLayout;
	static Context context;
	public ImageView header_logo_layout;
	public BrandTextView headerttnew;
	ProgressBarCircularIndeterminate circularIndeterminate;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		LayoutInflater inflator = (LayoutInflater) this .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflator.inflate(R.layout.actionbar_layout, null);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		//getActionBar().hide();
		actionBar =getActionBar();
		actionBar.hide();


		//actionBar.show();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_bg));
		//actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_bg));
		actionBar.setCustomView(v, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		setContentView(R.layout.standart_layout_fragment);
		actionBar.show();
		contentHolder=(LinearLayout)findViewById(R.id.contentholder);


		headerLayout = (RelativeLayout)v.findViewById(R.id.header_layout);
		headerMenu= (RelativeLayout)v.findViewById(R.id.header_menu);
		headerText = (BrandTextView)v.findViewById(R.id.header_text);
		overFlowButton=(ImageView)v.findViewById(R.id.over_flow_button);
		headerttnew=(BrandTextView)v.findViewById(R.id.header_text_t);
		header_logo_layout=(ImageView)v.findViewById(R.id.header_logo_layout);
		header_logo_layout.setVisibility(View.GONE);
		backLayout=(RelativeLayout)v.findViewById(R.id.back_layout);
		notification=(ImageView)v.findViewById(R.id.notification);
		notification.setVisibility(View.GONE);
		 circularIndeterminate=(ProgressBarCircularIndeterminate)findViewById(R.id.progressBarCircularIndetermininate);
		Typeface face = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Light.ttf");
		headerText.setTypeface(face);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		headerLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager)getSystemService(
						Context.INPUT_METHOD_SERVICE);

				imm.hideSoftInputFromWindow(headerLayout.getWindowToken(), 0);
				slidingMenu.toggle();
			}
		});
	}

	public void closeProgress(){
		circularIndeterminate.setVisibility(View.GONE);
	}
	
	public void openProgress(){
		circularIndeterminate.setVisibility(View.VISIBLE);
	}
	
	public LinearLayout getContentHolder() {
		return contentHolder;
	}
	public void setContentHolder(LinearLayout contentHolder) {
		this.contentHolder = contentHolder;
	}
	public void setHeaderText(String s){
		headerText.setText(s);
		headerText.setVisibility(View.VISIBLE);
	}
	public void hideHeaderText(){
		headerText.setText("");
		headerText.setVisibility(View.INVISIBLE);
	}
	public void hideHeaderMenu(){

		headerMenu.setVisibility(View.INVISIBLE);
	}

	public void hideHeaderLayout(){
		headerLayout.setVisibility(View.GONE);
	}
	public void setSlidingMenu(){
		slidingMenu = new SlidingMenu(this);
		slidingMenu.setMode(SlidingMenu.LEFT);
		slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		slidingMenu.setFadeDegree(0.35f);
		slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		slidingMenu.setMenu(R.layout.slidingmenu);

	}

	
	public void  showProgress(){
		
		
		
	}

	
	
}


