package com.example.barut.petmatch;

/**
 * Created by barut on 28.05.2017.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewPagerAdapterUser extends FragmentPagerAdapter {


    LayoutOne one;
    LayoutTwo two;

    public ViewPagerAdapterUser(Context context, FragmentManager fm) {
        super(fm);

    }
    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch(position){
            case 0:
                //  f=LayoutOne.newInstance(_context);

                one=new LayoutOne();

                return (Fragment)one;

            //break;
            case 1:
                //f=LayoutTwo.newInstance(_context);
                two=new LayoutTwo();

                return (Fragment)two;

            default:
                return null;
        }

    }
    @Override
    public int getCount() {
        return 2;
    }

}

