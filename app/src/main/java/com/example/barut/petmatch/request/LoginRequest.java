package com.example.barut.petmatch.request;

import android.os.AsyncTask;


import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginRequest extends AsyncTask<Void, Void, String> {

	private String Password;
	private String Email;
	private AsyncListener asc;

	public LoginRequest(String Email, String Password, AsyncListener asc) {
		super();
		this.asc = asc;
		try {
//
			this.Password =Password;
			this.Email = Email;
			execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected String doInBackground(Void... params) {

		JSONObject jsonObject=new JSONObject();

		try {

			jsonObject.put("password", Password);
			jsonObject.put("eMail", Email);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_USERS + "login", jsonObject);


	}
	@Override
	protected void onPostExecute(String result) {

		try {
			JSONObject jsonObj = new JSONObject(result);
			
			asc.asyncOperationSucceded(jsonObj, RequestsQueue.LOGIN, null);
		} catch (JSONException e) {
			e.printStackTrace();
			asc.asyncOperationFailed(null, RequestsQueue.LOGIN);
		}



		super.onPostExecute(result);
	}
}
