package com.example.barut.petmatch.dataobject;

/**
 * Created by barut on 21.03.2017.
 */
public class Disease {
    int _id;				// string
    String diseaseName;		// string
    boolean isCurrent;				// boolean

public Disease() {}
    public Disease(String diseaseName, boolean isCurrent) {
        this.diseaseName = diseaseName;
        this.isCurrent = isCurrent;
    }

    public Disease(int _id, String diseaseName, boolean isCurrent) {
        this._id = _id;
        this.diseaseName = diseaseName;
        this.isCurrent = isCurrent;
    }
}
