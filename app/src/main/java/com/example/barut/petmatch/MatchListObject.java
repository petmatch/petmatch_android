package com.example.barut.petmatch;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;

import org.apache.http.impl.cookie.BasicMaxAgeHandler;

import loadimage.ImageLoader;

/**
 * Created by barut on 28.05.2017.
 */

public class MatchListObject extends RelativeLayout implements AsyncListener {
    public BrandTextView petName;
    public String petNameString;
    public CircularImageView petIcon;
    public RelativeLayout disL;
    boolean bool=false;
    String path;
    public String id;
    private String layout;
   private  Context context;
    public MatchListObject(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public MatchListObject(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }
    public static MatchListObject getObject(Context context, String petNameString, String  photopath, String petId ){

        LayoutInflater inflater = (LayoutInflater)context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        MatchListObject mio;
        mio = (MatchListObject) inflater.inflate(R.layout.match_object_layout, null);
        mio.context=context;
        mio.petNameString=petNameString;
        mio.path=photopath;
        mio.id=petId;



        return mio;
    }

    private Boolean isSettled = false;
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        // TODO Auto-generated method stub
        super.onSizeChanged(w, h, oldw, oldh);
        setupLayout();

    }

    public void setupLayout(){
        if(!isSettled){
            isSettled = true;
            petName = (BrandTextView)findViewById(R.id.match_name);
            petIcon = (CircularImageView)findViewById(R.id.match_profile_icon);
            disL=(RelativeLayout)findViewById(R.id.dis);

            petName.setText(petNameString);

            petName.setTextColor(getResources().getColor(R.color.gray_text));
            ImageLoader img=new ImageLoader(context.getApplicationContext());
            img.DisplayImage(path,petIcon);


            disL.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent i=new Intent(context.getApplicationContext(),UserActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Utils.ownAccount=false;
                    Utils.fromMyMatches=true;
                    Utils.petID=id;
                    context.startActivity(i);
                }
            });



        }




    }

    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {


    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        // TODO Auto-generated method stub

    }


}
