package com.example.barut.petmatch.request;

import android.os.AsyncTask;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by lENOVO on 31.05.2017.
 */

public class AddNewPetRequest extends AsyncTask<Void, Void, String> {
    private String token="";
    AsyncListener asc;
    private String petName="";
    private String petType="";
    private String petCity="";
    private int petGender;
    private int petAge;
    private boolean isSterilized=false;
   // private ArrayList<String>   prefType=new ArrayList<String>();
    private  String vetMail="";
    private String vetPhone="";

    public AddNewPetRequest(String token, String petName, String petType, String petCity, int petGender,
                            int petAge, boolean isSterilized, String vetMail,String vetPhone, AsyncListener asc) {
        this.token = token;
        this.asc=asc;
        this.petName=petName;
        this.petType=petType;
        this.petCity=petCity;
        this.petGender=petGender;
        this.petAge=petAge;
        this.isSterilized=isSterilized;
        this.vetMail=vetMail;
        this.vetPhone=vetPhone;
        execute();
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("token",token);
            jsonObject.put("petName",petName);
            jsonObject.put("petType",petType);
            jsonObject.put("petGender",petGender);
            jsonObject.put("petAge",petAge);
            jsonObject.put("isSterilized",isSterilized);
            jsonObject.put("petCity",petCity);
            jsonObject.put("vetMail",vetMail);
            jsonObject.put("vetPhone",vetPhone);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_PETS + "addPet", jsonObject);

    }

    @Override
    protected void onPostExecute(String result) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.ADD_NEW_PET, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.ADD_NEW_PET);
        }
        super.onPostExecute(result);
    }
}
