package com.example.barut.petmatch;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.barut.petmatch.request.LogoutRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.barut.petmatch.Dashboard.petArrayList;

public class SlidingMenuFragment extends Fragment implements AsyncListener{
    private View view;
    private RelativeLayout menuLastest;
    private RelativeLayout  user;
    private RelativeLayout menuAdopt;
    private RelativeLayout menuMatch;
    private RelativeLayout menuMyMatches;
    private RelativeLayout menuSettings;
    private LinearLayout menuLayout;
    private LinearLayout userLAyout;
    private LinearLayout logOutLayout;
    private RelativeLayout menuCikis;
    SharedPreferences pref;
    public static final String MyPREFERENCES = "MyPrefs" ;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.slidingmenu_fragment, container, false);

        pref=this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        menuLayout=(LinearLayout)view.findViewById(R.id.menu_layout);
        userLAyout=(LinearLayout)view.findViewById(R.id.user_layout);

        if(pref.contains("PetName")) {
            user=MenuItemObject.getObject(getActivity(),pref.getString("PetName",""),R.drawable.profile);
            userLAyout.addView(user);
        }
        else {
            user = MenuItemObject.getObject(getActivity(), "Pet Profile" , R.drawable.profile);
            userLAyout.addView(user);
        }
        menuLastest=MenuItemObject.getObject(getActivity(), "Ana Sayfa",R.drawable.dashboard);
        //menuCikis=MenuItemObject.getObject(getActivity(),"Log Out",R.drawable.logout_ikon);
        menuSettings=MenuItemObject.getObject(getActivity(), "Ayarlar", R.drawable.ayarlar);
        menuMyMatches=MenuItemObject.getObject(getActivity(), "My Matches",R.drawable.action_button);
        menuLayout.addView(menuLastest);
        menuAdopt=MenuItemObject.getObject(getActivity(), "Adopt",R.drawable.adopt);
        menuMatch=MenuItemObject.getObject(getActivity(), "Match", R.drawable.match);
        menuLayout.addView(menuAdopt);
        menuLayout.addView(menuMatch);
        menuLayout.addView(menuSettings);
        menuLayout.addView(menuMyMatches);
        //menuLayout.addView(MenuItemObject.getObject(getActivity(), "Ayarlar",R.drawable.ayarlar));
      //  menuLayout.addView(menuCikis);
        logOutLayout=(LinearLayout)view.findViewById(R.id.content_layout) ;
        logOutLayout.addView(MenuItemObject.getObject(getActivity(), "LogOut", R.drawable.logout_ikon));
        menuSettings.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), AccountSettings.class);
				/*intent.putExtra("userIconPath",);
				intent.putExtra("userCoverPath",pref.getString("CoverPhotoPath",""));
				intent.putExtra("Id",pref.getString("userId",""));*/
                startActivity(intent);
            }
        });
      /*  menuCikis.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Editor editor = pref.edit();
                editor.putBoolean("isLogin", false);
                Utils.isLogin=false;
                editor.putString("name", "");
                editor.putString("userId", "");
                editor.commit();
                Intent intent=new Intent(getActivity(), Dashboard.class);
                startActivity(intent);
            }
        }); */

        logOutLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new LogoutRequest(Utils.token, SlidingMenuFragment.this);


            }
        });

        menuMatch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (Utils.petNum==0) {
                        final MaterialDialog materialDialog = new MaterialDialog(getActivity());
                        materialDialog.setMessage("You first need to add a pet to match it!")
                                .setPositiveButton("Add Pet", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        materialDialog.dismiss();
                                        Intent intent=new Intent(getActivity(), AddAPetActivity.class);
                                        startActivity(intent);
                                    }
                                });
                                materialDialog.setNegativeButton("Later", new OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                materialDialog.dismiss();
                                            }
                                        }

                                );
                        materialDialog.show();
                    } else {
                        Intent intent = new Intent(getActivity(), LikeOrNotActivity.class);
                        intent.putExtra("flag","match");
                        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }
                catch(Exception n) {
                    n.printStackTrace();
                }


            }
        });


        user.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if(Utils.petNum==0) {
                    final MaterialDialog materialDialog = new MaterialDialog(getActivity());
                    materialDialog.setMessage("You first need to add a pet to see it's profile!")
                            .setPositiveButton("Add Pet", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    materialDialog.dismiss();
                                    Intent intent=new Intent(getActivity(), AddAPetActivity.class);
                                    startActivity(intent);
                                }
                            });
                    materialDialog.setNegativeButton("Later", new OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    materialDialog.dismiss();
                                }
                            }

                    );
                    materialDialog.show();
                }
                else {
                    Intent intent = new Intent(getActivity(), UserActivity.class);
                    Utils.ownAccount=true;
                    Utils.fromMyMatches=false;
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }

            }
        });

        menuLastest.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent=new Intent(getActivity(), Dashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        menuAdopt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), LikeOrNotActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("flag","adopt");
                startActivity(intent);
            }
        });

        menuMyMatches.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getActivity(), MyMatches.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        return view;
    }


    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue, String extraData) {
        JSONObject object=(JSONObject) obj;
        try {
            if (object.getString("response") == "1") {
                Editor editor = pref.edit();
                editor.putBoolean("isLogin", false);
                Utils.isLogin = false;
                Utils.token="";
                editor.putString("name", "");
                editor.putString("userId", "");
                editor.putString("token","");
                editor.commit();
                Intent intent = new Intent(getActivity(), Dashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            else {
                Toast.makeText(getActivity(),"Log Out Failed",Toast.LENGTH_SHORT);
            }


        } catch(JSONException J) {
            J.printStackTrace();
        }
    }

    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {

    }
}