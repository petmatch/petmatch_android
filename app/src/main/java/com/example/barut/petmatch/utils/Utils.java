package com.example.barut.petmatch.utils;

import android.content.Intent;
import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by barut on 09.03.2017.
 */
public class Utils {
    public static boolean justDeletedPet=false;
    public static boolean ownAccount=true;
    public static boolean fromMyMatches=false;
    public static boolean isLogin=false;
    public static boolean isPremium=false;
    public static String userId="";
    public static String username="";
    public static String name="";
    public static String city;
    public static String petCity="";
    public static String profilephoto="";
    public static String coverphto="";
    public static String petprofilephoto="";
    public static String petcoverphto="";
    public static String surname="";
    public static String eposta="";
    public static String phone="";
    public static Bitmap profile;
    public static Bitmap cover;
    public static Bitmap petprofile;
    public static Bitmap petcover;
    public static String biography="";
    public static int layoutwidth;
    public static int drawableProfilePath;
    public static int drawableCoverPath;
    public static int petdrawableProfilePath;
    public static int petdrawableCoverPath;
    public static String petname="";
    public static String token="";
    public static String vetMail="";
    public static String vetPhone="";
    public static String petID="";
    public static boolean isSterilized= false; 	// boolean
    public static String petbio="";			// string
    public static String petType; 				// string
    public static int petGender; 				// string
    public static int petAge; 					// integer
    public static int petNum;
    public static boolean pendingAdoption;
    public static boolean pendingMatch;
    public static String preff1="BullDog";
    public static String preff2="Golden Retriever";
    public static String preff3="Pekineese";

}
