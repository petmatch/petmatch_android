package com.example.barut.petmatch.request;

import android.os.AsyncTask;

import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.service.ServiceManager;
import com.example.barut.petmatch.utils.ApplicationConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by barut on 22.03.2017.
 */
public class UploadProfilePictureRequest extends AsyncTask<Void, Void, String> {
    private String sessionKey;
    private String pictureURL;

    private AsyncListener asc;
        public UploadProfilePictureRequest(String sessionKey,String pictureURL, AsyncListener asc) {
        super();
        try {
            this.asc = asc;
            this.sessionKey = sessionKey;
            this.pictureURL=pictureURL;
            execute();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

        @Override
        protected String doInBackground(Void... params) {
        JSONObject jsonObject=new JSONObject();
        try {


            jsonObject.put("sessionKey", sessionKey);
            jsonObject.put("pictureURL", pictureURL);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        return ServiceManager.sendPOST(ApplicationConstants.SERVER_ADDRESS_USERS + "api/UploadProfilePicture", jsonObject);
    }

        @Override
        protected void onPostExecute(String result) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(result);
            asc.asyncOperationSucceded(jsonObj, RequestsQueue.UPLOAD_PROFILE_PICTURE, null);
        } catch (Exception e) {
            e.printStackTrace();
            asc.asyncOperationFailed(jsonObj, RequestsQueue.UPLOAD_PROFILE_PICTURE);
        }
        super.onPostExecute(result);
    }
}
