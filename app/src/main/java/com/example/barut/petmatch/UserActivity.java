package com.example.barut.petmatch;


import android.os.Bundle;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import loadimage.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.example.barut.petmatch.request.GetPetFullRequest;
import com.example.barut.petmatch.service.AsyncListener;
import com.example.barut.petmatch.service.RequestsQueue;
import com.example.barut.petmatch.utils.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

//import com.squareup.picasso.Picasso;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class UserActivity extends BaseFragmentActivity implements AsyncListener,OnRefreshListener<ScrollView>{
    private ViewPager pager;
    private	ViewPagerAdapterUser adapter;
    private	BrandTextView textView11;
    private	BrandTextView textView12;
    public static	BrandTextView textView21;
    private	BrandTextView textView22;
    public static	BrandTextView textView31;

    private	BrandTextView userNAme;
    private ImageView coverPhotoPath;
    private ImageView settingsIcon;
    private RelativeLayout layout;
    private BrandTextView biographyTextview;
    private	View info;
    private	View add;
    private LinearLayout biographyLayout;
    private TableLayout tabs;
    private ImageView cancelBiography;
    private CircularImageView userProfileImage;
    private LinearLayout indicator;
    private ImageView notifi;
    boolean bool=false;
    public static String userId;
    private BrandTextView textViewusername;
    private int leftTemp;
    private int rightTemp;
    ImageLoader imgLoader;
    ImageView back;



    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences pref;
    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        pref=this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tvw = (View)inflater.inflate(R.layout.activity_user, null);
        pager=(ViewPager)tvw.findViewById(R.id.viewPager);
        textView11=(BrandTextView)tvw.findViewById(R.id.textView11);
        textView12=(BrandTextView)tvw.findViewById(R.id.textView12);
        textView21=(BrandTextView)tvw.findViewById(R.id.textView21);
        textView22=(BrandTextView)tvw.findViewById(R.id.textView22);
        settingsIcon=(ImageView)tvw.findViewById(R.id.setttings_icon);
        back=(ImageView)tvw.findViewById(R.id.back);
        biographyTextview=(BrandTextView)tvw.findViewById(R.id.biography_textview);
        textViewusername=(BrandTextView)tvw.findViewById(R.id.user_username);
        userProfileImage = (CircularImageView)tvw.findViewById(R.id.profile_image);
        userNAme = (BrandTextView)tvw.findViewById(R.id.user_name);
        coverPhotoPath=(ImageView)tvw.findViewById(R.id.cover_photo);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        coverPhotoPath.getLayoutParams().height=width*3/4;

        layout = (RelativeLayout)tvw.findViewById(R.id.layout);
        info=(View)tvw.findViewById(R.id.info);

        add=(View)tvw.findViewById(R.id.add);
        biographyLayout=(LinearLayout)tvw.findViewById(R.id.user_biography_layout);

        tabs=(TableLayout)tvw.findViewById(R.id.tabs);
        cancelBiography=(ImageView)tvw.findViewById(R.id.cancel_biography);
        indicator=(LinearLayout)tvw.findViewById(R.id.indicator);
        textView11.setTypeface(Typeface.createFromAsset(UserActivity.this.getAssets(), "fonts/Roboto-Bold.ttf"));
        textView21.setTypeface(Typeface.createFromAsset(UserActivity.this.getAssets(), "fonts/Roboto-Bold.ttf"));
        getContentHolder().addView(tvw);
        setUpView();
        setTab();
        actionBar.hide();
        cancelBiography.setColorFilter(R.color.app);
        pager.setOffscreenPageLimit(4);
        imgLoader = new ImageLoader(this);

        Intent intent=getIntent();
        String toast= intent.getStringExtra("toast");
        if(toast!=null)
        {
            Toast.makeText(UserActivity.this,toast,Toast.LENGTH_SHORT).show();
        }


        if(Utils.ownAccount) {
            userProfileImage.setImageResource(R.drawable.bulldog);
            coverPhotoPath.setImageResource(R.drawable.coverphoto);
        }
        else {
            imgLoader.DisplayImage(Utils.petprofilephoto,userProfileImage);
            coverPhotoPath.setImageResource(R.drawable.coverphoto);
            settingsIcon.setVisibility(View.INVISIBLE);

        }
        if(Utils.fromMyMatches) {
            new GetPetFullRequest(Utils.petID,UserActivity.this);
        }

      //  openProgress();
        settingsIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent=new Intent(UserActivity.this, PetProfileSettings.class);
                startActivity(intent);

            }
        });


        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });


        info.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                biographyLayout.setVisibility(View.VISIBLE);
                pager.setVisibility(View.INVISIBLE);
                tabs.setVisibility(View.INVISIBLE);
                indicator.setVisibility(View.INVISIBLE);
            }
        });

        cancelBiography.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biographyLayout.setVisibility(View.INVISIBLE);
                pager.setVisibility(View.VISIBLE);
                tabs.setVisibility(View.VISIBLE);
                indicator.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setTab(){
        pager.setOnPageChangeListener(new OnPageChangeListener(){

            @Override
            public void onPageScrollStateChanged(int position) {
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}
            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub

                System.out.println(position);
                switch(position){
                    case 0:

                        findViewById(R.id.first_tab).setVisibility(View.VISIBLE);
                        findViewById(R.id.second_tab).setVisibility(View.INVISIBLE);

                        textView11.setTextColor(Color.rgb(252, 127, 106));
                        textView12.setTextColor(Color.rgb(252, 127, 106));
                        textView21.setTextColor(Color.rgb(255, 255, 255));
                        textView22.setTextColor(Color.rgb(255, 255, 255));

                        break;

                    case 1:




                        findViewById(R.id.first_tab).setVisibility(View.INVISIBLE);
                        findViewById(R.id.second_tab).setVisibility(View.VISIBLE);
                        textView11.setTextColor(Color.rgb(255, 255, 255));
                        textView12.setTextColor(Color.rgb(255, 255, 255));
                        textView21.setTextColor(Color.rgb(252, 127, 106));
                        textView22.setTextColor(Color.rgb(252, 127, 106));


                        break;


                }
            }

        });
//		adapter.one.mPullRefreshScrollView.setOnRefreshListener(new OnRefreshListener<ScrollView>() {
//			@Override
//			public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
//				arrayListUserPhoto.removeAll(arrayListUserPhoto);
//				currentP++;
//				new GetUsersPhotoRequest(UserActivity.this, userId,Integer.toString(currentP));
//			}
//		});
    }
    private void setUpView(){
        adapter = new ViewPagerAdapterUser(getApplicationContext(),getSupportFragmentManager());
        pager.setAdapter(adapter);
        textView11.setTextColor(Color.rgb(252, 127, 106));
        textView12.setTextColor(Color.rgb(252, 127, 106));
        pager.setCurrentItem(0);
    }


    @SuppressWarnings("deprecation")
    @Override
    public void asyncOperationSucceded(Object obj, RequestsQueue queue,
                                       String extraData) {


        }



    public void run() {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isLogin", true);
        editor.putString("name", Utils.name);
        editor.putString("surname", Utils.surname);
        editor.putString("userId", Utils.userId);
        editor.putString("UserName", Utils.username);
        editor.putString("ProfilePhotoPath", Utils.profilephoto);
        editor.putString("CoverPhotoPath", Utils.coverphto);
        editor.putString("Email", Utils.eposta);
        editor.putString("MobilePhone", Utils.phone);
        editor.commit();
    }
    @Override
    public void asyncOperationFailed(Object obj, RequestsQueue queue) {
        System.out.println("dönen bu " +obj);



    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        RelativeLayout bmImage;

        public DownloadImageTask(RelativeLayout bmImage) {
            this.bmImage = bmImage;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            //  pd.show();
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                //   Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            //     pd.dismiss();
            Drawable d =new BitmapDrawable(getResources(),result);

            bmImage.setBackgroundDrawable(d);
        }
    }
    @Override
    public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
        // TODO Auto-generated method stub

    }
}
