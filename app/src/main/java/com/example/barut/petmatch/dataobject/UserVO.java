package com.example.barut.petmatch.dataobject;

import android.util.Log;

import com.example.barut.petmatch.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class UserVO implements Serializable {
	/**
	 * 
	 */

	//petList: 						// pet object(s)
	//adoptedList:							// pet object(s)

	/*profilePicture:								// picture object
	{
		_id: “2348752”;							// string
		link: “petmatch.com/pics/altug_the_java_developer.jpg”;		// string
		width: 400;								// integer
		height: 500;								// integer
	}*/

	/*Preferences:				// preference object
	{
		minPetAge: 3;				// integer
		maxPetAge: 4;				// integer
		gender: “male”;				// string
		city: “İzmir”;				// string
		petType[3]:				// ArrayList<String>
		{
			petType[0]: “Golden”;				// string
			petType[1]: “Bulldog”;			// string
			petType[2]: ”Rottweiler”;			// string
		}
	}*/



	/////



	public List<Pet> petList = new ArrayList<Pet>();

	public UserVO(JSONObject dict){
		try {
			if(dict.has("token"))
				Utils.token= dict.getString("token");
			if(dict.has("ownerName"))
				Utils.name=dict.getString("ownerName");

			/*if(dict.has("petList")) {
				JSONArray array = dict.getJSONArray("petList");
				for(int i = 0 ; i < array.length() ; i++){
					petList.add(array.getJSONObject(i).getObject("interestKey"));
				}
				petList = dict.get("petList");
			}*/
			if(dict.has("city"))
				Utils.city=dict.getString("city");
			if(dict.has("eMail"))
				Utils.eposta=dict.getString("eMail");
			if(dict.has("phone"))
				Utils.phone=dict.getString("phone");
			if(dict.has("ProfilePhotoPath")&&dict.getString("ProfilePhotoPath")!=null)
				Utils.profilephoto = dict.getString("ProfilePhotoPath");
			//if(dict.has("birthday"))
			//	birthday=dict.getString("birthday");
			if(dict.has("isPremium"))
				Utils.isPremium=dict.getBoolean("isPremium");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
