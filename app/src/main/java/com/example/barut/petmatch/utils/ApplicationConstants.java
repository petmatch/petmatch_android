package com.example.barut.petmatch.utils;

/**
 * Created by barut on 09.03.2017.
 */
public class ApplicationConstants {
    public static final String SERVER_ADDRESS_USERS ="http://192.168.43.34:8080/PetMatch_Server/service/users/";
    public static final String SERVER_ADDRESS_PETS ="http://192.168.43.34:8080/PetMatch_Server/service/pets/";
    public static final String SERVER_ADRESS_PHOTOS = "https://pixabay.com/api/?key=";
    public static final String pixabayKey="5392446-b743411f2f7750021733f235e";
    public static final String SERVER_ADRESS_DOG_PHOTOS =SERVER_ADRESS_PHOTOS+pixabayKey+"&q=dogs&image_type=photo&";
}
