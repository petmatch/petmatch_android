package com.example.barut.petmatch.dataobject;

/**
 * Created by barut on 21.03.2017.
 */
public class Photo {
    boolean isProfilePhoto=false;
    String _id;					// string
    String link;	// string
    int width;						// integer
    int height;
    public Photo() {

    }
    public Photo( String link, int width, int height) {
        this.link = link;
        this.width = width;
        this.height = height;
    }
    public Photo(String _id, String link, int width, int height) {
        this._id = _id;
        this.link = link;
        this.width = width;
        this.height = height;
    }
}
